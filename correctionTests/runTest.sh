TEST_NAME=$1

ROOT_PATH="."
RESULT_ROOT="/tmp"
RESULT_ORIGINAL="${RESULT_ROOT}/malloc_${TEST_NAME}_original.txt"
RESULT_CUSTOM="${RESULT_ROOT}/malloc_${TEST_NAME}_custom.txt"

SPECIAL_TEST="test5"

if [ "$(uname)" = "Linux" ]; then
	if [ "${SPECIAL_TEST}" = "${TEST_NAME}" ];then
		rm ${TEST_NAME}
		gcc -o ${SPECIAL_TEST} "${SPECIAL_TEST}.c" -L. -lft_malloc -I ./inc/

		${ROOT_PATH}/run_manjaro.sh ${ROOT_PATH}/${TEST_NAME} 3>/dev/null &> ${RESULT_CUSTOM}
		
		echo "Result with custom malloc"
		echo "========================="
		echo ""
		cat ${RESULT_CUSTOM}
	else	
		${ROOT_PATH}/compile.sh ${TEST_NAME}

		${ROOT_PATH}/${TEST_NAME} &> ${RESULT_ORIGINAL}

		${ROOT_PATH}/run_manjaro.sh ${ROOT_PATH}/${TEST_NAME} 3>/dev/null &> ${RESULT_CUSTOM}

		echo "Result with malloc"
		echo "=================="
		echo ""
		cat ${RESULT_ORIGINAL}
		echo ""
		echo ""
		echo "Result with custom malloc"
		echo "========================="
		echo ""
		cat ${RESULT_CUSTOM}
	fi
else
	if [ "${SPECIAL_TEST}" = "${TEST_NAME}" ];then
		rm ${TEST_NAME}
		gcc -o ${SPECIAL_TEST} "${SPECIAL_TEST}.c" -L. -lft_malloc -I ./inc/

		${ROOT_PATH}/run.sh /usr/bin/time -l ${ROOT_PATH}/${TEST_NAME} 3>/dev/null &> ${RESULT_CUSTOM}
		
		echo "Result with custom malloc"
		echo "========================="
		echo ""
		cat ${RESULT_CUSTOM}
	else	
		${ROOT_PATH}/compile.sh ${TEST_NAME}

		/usr/bin/time -l ${ROOT_PATH}/${TEST_NAME} &> ${RESULT_ORIGINAL}

		${ROOT_PATH}/run.sh /usr/bin/time -l ${ROOT_PATH}/${TEST_NAME} 3>/dev/null &> ${RESULT_CUSTOM}

		echo "Result with malloc"
		echo "=================="
		echo ""
		cat ${RESULT_ORIGINAL}
		echo ""
		echo ""
		echo "Result with custom malloc"
		echo "========================="
		echo ""
		cat ${RESULT_CUSTOM}
	fi
fi
