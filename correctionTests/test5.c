#include "malloc.h"

int main()
{
	write(1, "test5\n", 6);
//printf("struct -> %p\n", g_malloc_struct);
//printf("allocationTracker -> %d\n", g_malloc_struct->allocation_tracker);
//printf("memory_tiny -> %p\n", g_malloc_struct->memory_tiny);
//printf("memory_small -> %p\n", g_malloc_struct->memory_small);
//printf("memory_large -> %p\n", g_malloc_struct->memory_large);
//printf("data_memory_handler -> %p\n", g_malloc_struct->data_memory_handler);

malloc(1024);
malloc(1024 * 32);
malloc(1024 * 1024);
malloc(1024 * 1024 * 16);
malloc(1024 * 1024 * 128);

show_alloc_mem();
return (0);
}
/*
	int						allocation_tracker;
	t_heap					*memory_tiny;
	t_heap					*memory_small;
	t_heap					*memory_large;
	t_data_memory_handler	*data_memory_handler;
	unsigned int			has_been_initialized;
	*/