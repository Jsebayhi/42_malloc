#ifndef TESTS_H
# define TESTS_H

# include "libft.h"
# include "stdio.h"
# include "unitTestFramework.h"

// imported not public prototypes
int    allocation_counter(int newValue, int apply);
int    allocation_counter_get();

// tests
int		memory_handler_test(int fd, char *prefix);
int		t_data_test(int fd, char *prefix);
int		t_data_memoryHanlder_test(int fd, char *prefix);
int		t_heap_test(int fd, char *prefix);
int		t_malloc_test(int fd, char *prefix);
int		malloc_test(int fd, char *prefix);

#endif