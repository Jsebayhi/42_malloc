#ifndef UNIT_TEST_FRAMEWORK_H
# define UNIT_TEST_FRAMEWORK_H

# include "libft.h"

# define TAB "    "
# ifndef FD_DEBUG
#  define FD_DEBUG 3
# endif

typedef int TestSuit(int, char *);
typedef TestSuit *TestSuits;

// function which run all the tests.
void	runTestsSuits(int fd, TestSuits testSuits);

// internal function
int		runTest(int fd, char *txtPrefix, char *testName, int test(int, char *));

// function to use in your test suits.
void	printTestName(int fd, char *prefix, char *testName);
void	printTestNameEndl(int fd, char *prefix, char *testName);
int		assert(int fd, int boolean);
int		assertEndln(int fd, int boolean);
void	fatal(int fd, char	*msg);

#endif