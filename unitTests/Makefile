#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/03 19:15:09 by jsebayhi          #+#    #+#              #
#    Updated: 2019/03/16 15:08:20 by jsebayhi         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME = 42_malloc_tests_unit

CC = clang
CFLAGS = -Wall -Wextra -Werror -g

INC_PATH = ../malloc/inc/\
			./inc/\

SRC_PATH = ./src/

OBJ_PATH = ./.obj/

SRC_NAME = unitTestFramework.c\
			main.c\
			memory_handler_test.c\
			s_data_test.c\
			s_data_memory_handler_test.c\
			s_heap_test.c\
			s_malloc_test.c\
			_malloc_test.c\
			
OBJ_NAME = $(SRC_NAME:.c=.o)

LIB_MALLOC_PATH=../malloc/
LIB_MALLOC_NAME=-lft_malloc


SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
LIB = $(addprefix -L,$(LIB_MALLOC_PATH))
INC = $(addprefix -I,$(INC_PATH))

.DEFAULT_GOAL := help

all: lib_make $(NAME) ## make needed lib

$(NAME):$(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $(LIB) $(LIB_NAME) $(LIB_MALLOC_NAME) $(INC) -o $(NAME)

$(OBJ_PATH)%.o:$(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean: ## clean object files
	rm -rfv $(OBJ_PATH)

fclean: clean fclean_lib ## apply 'clean' rule then remove executable
	rm -fv $(NAME)

re: fclean all ## apply 'clean' then 'all' rule


# LIB MALLOC 

lib_malloc: ## make malloc as a library in order for it to be tested
	make as_lib clean -C $(LIB_MALLOC_PATH)

lib_malloc_fclean: ## fclean the malloc lib
	make fclean -C $(LIB_MALLOC_PATH)

# ALL LIB
lib_make: lib_malloc ## make all lib
#	for lib in $(LIB_PATH); do \
#	make -C $$lib ;\
#	done

fclean_lib: lib_malloc_fclean ## clean all lib
#	for lib in $(LIB_PATH); do \
#	make fclean -C $$lib ;\
#	done

relib: fclean_lib lib_make ## remake all lib

.PHONY: help
# rules' name must have a length inferior to 25.
# credit: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## show available commands and description
	@grep -h -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'