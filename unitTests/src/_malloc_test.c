#include "tests.h"
#include "malloc.h"
#include <string.h>

int		malloc_test(int fd, char *prefix) {
	//preparing tests
	int error_nb = 0;
	char *new_prefix = prefix;
	allocation_counter_set(0);

	//tests
	printTestNameEndl(fd, new_prefix, "series 1 - simple case.");
	{
		printTestNameEndl(fd, new_prefix, "test1 - Instanciation and destruction of malloc.");
		printTestName(fd, new_prefix, "test1 - instanciating malloc: ");
		void	*malloced;
		malloced = malloc_sub(2);
		error_nb += assertEndln(fd, NULL != malloced);
		
		//_show_alloc_mem();

		printTestName(fd, new_prefix, "test1 - reallocing: ");

		void	*realloced;
		realloced = realloc_sub(malloced, 1000);
		error_nb += assertEndln(fd, NULL != realloced);
		
		//_show_alloc_mem();

		printTestNameEndl(fd, new_prefix, "test1 - trying deleting bad pointer from realloc pointer: OK");
		free_sub((void*)realloced + 4);

		printTestNameEndl(fd, new_prefix, "test1 - deleting realloc: OK");
		free_sub(realloced);
		
		//_show_alloc_mem();

		printTestName(fd, new_prefix, "test1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
/*
	printTestNameEndl(fd, new_prefix, "series 2 - malloc.");
	printTestNameEndl(fd, new_prefix, "series 3 - calloc.");
	printTestNameEndl(fd, new_prefix, "sub series 1 - malloc common rules.");
	printTestNameEndl(fd, new_prefix, "sub series 1 - calloc specificities.");
	{
		printTestNameEndl(fd, new_prefix, "test1 - allocate the size of 2 * 3.");
		//_calloc(2, 3)
	}
	printTestNameEndl(fd, new_prefix, "series 4 - free.");


	printTestNameEndl(fd, new_prefix, "series 5 - realloc.");
	printTestNameEndl(fd, new_prefix, "sub series 1 - edge case.");
	{
		printTestNameEndl(fd, new_prefix, "test1 - reallocation of a Null pointer.");
	}
	printTestNameEndl(fd, new_prefix, "sub series 2 - from tiny.");
	{
		printTestNameEndl(fd, new_prefix, "test1 - reallocation of a tiny pointer to a size of 0.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test2 - reallocation of a tiny pointer to a smaller tiny size should copy as much as possible.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test3 - reallocation of a tiny pointer as a tiny should return the same pointer when there are space to expend it.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test4 - reallocation of a tiny pointer as a medium should return a new pointer containing the previous data.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test5 - reallocation of a tiny pointer as a large should return a new pointer containing the previous data.");
	}
	printTestNameEndl(fd, new_prefix, "sub series 3 - from medium.");
	{
		printTestNameEndl(fd, new_prefix, "test1 - reallocation of a medium pointer to a size of 0.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test2 - reallocation of a medium pointer to a smaller tiny size should copy as much as possible.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test3 - reallocation of a medium pointer to a smaller medium size should copy as much as possible.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test4 - reallocation of a medium pointer as a tiny should return the same pointer when there are space to expend it.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test5 - reallocation of a medium pointer as a medium should return a new pointer containing the previous data.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test6 - reallocation of a medium pointer as a large should return a new pointer containing the previous data.");
	}
	printTestNameEndl(fd, new_prefix, "sub series 4 - from large.");
	{
		printTestNameEndl(fd, new_prefix, "test1 - reallocation of a large pointer to a size of 0.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test2 - reallocation of a large pointer to a smaller tiny size should copy as much as possible.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test3 - reallocation of a large pointer to a smaller medium size should copy as much as possible.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test4 - reallocation of a large pointer to a smaller medium size should copy as much as possible.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test5 - reallocation of a large pointer as a tiny should return the same pointer when there are space to expend it.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test6 - reallocation of a large pointer as a medium should return a new pointer containing the previous data.");
	}
	{
		printTestNameEndl(fd, new_prefix, "test7 - reallocation of a large pointer as a large should return a new pointer containing the previous data.");
	}
*/
	// realloc
	return (error_nb);
}