#include "tests.h"
#include "s_malloc.h"
#include <string.h>

int		t_malloc_test(int fd, char *prefix) {
	//preparing tests
	int error_nb = 0;
	char *new_prefix = prefix;
	allocation_counter_set(0);

	//tests
	{
		printTestNameEndl(fd, new_prefix, "test1 - Instanciation and destruction of malloc.");
		printTestName(fd, new_prefix, "test1 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		printTestNameEndl(fd, new_prefix, "test1 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestNameEndl(fd, new_prefix, "test2 - simple allocation of each available size.");
		printTestName(fd, new_prefix, "test2 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr1;
		size_t	ptr1Size = MALLOC_MEM_T_SIZE - 3;
		printTestName(fd, new_prefix, "test2 - allocating memory tiny (1):");
		ptr1 = malloc_allocate(s_malloc, ptr1Size);
		error_nb += assert(fd, NULL != ptr1);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, ptr1 == s_malloc->memory_tiny->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr1Size) == s_malloc->memory_tiny->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestName(fd, new_prefix, "test2 - releasing memory (1):");
		malloc_release(s_malloc, ptr1);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny->lst_data);
		error_nb += assertEndln(fd, 0 == s_malloc->allocation_tracker);

		void	*ptr2;
		size_t	ptr2Size = MALLOC_MEM_S_SIZE - 15;
		printTestName(fd, new_prefix, "test2 - allocating memory small (2):");
		ptr2 = malloc_allocate(s_malloc, ptr2Size);
		error_nb += assert(fd, NULL != ptr2);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, ptr2 == s_malloc->memory_small->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr2Size) == s_malloc->memory_small->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestName(fd, new_prefix, "test2 - releasing memory (2):");
		malloc_release(s_malloc, ptr2);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_small->lst_data);
		error_nb += assertEndln(fd, 0 == s_malloc->allocation_tracker);

		void	*ptr3;
		size_t	ptr3Size = MALLOC_MEM_S_SIZE + 34;
		printTestName(fd, new_prefix, "test2 - allocating memory large (3):");
		ptr3 = malloc_allocate(s_malloc, ptr3Size);
		error_nb += assert(fd, NULL != ptr3);
		error_nb += assert(fd, NULL != s_malloc->memory_large);
		error_nb += assert(fd, ptr3 == s_malloc->memory_large->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr3Size) == s_malloc->memory_large->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestName(fd, new_prefix, "test2 - releasing memory (3):");
		malloc_release(s_malloc, ptr3);
		error_nb += assert(fd, NULL != s_malloc->memory_large);
		error_nb += assert(fd, NULL == s_malloc->memory_large->lst_data);
		error_nb += assertEndln(fd, 0 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test2 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test2 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	printTestNameEndl(fd, new_prefix, "test3 - simple Reallocations of each available size.");
	{
		printTestName(fd, new_prefix, "test3.1 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr1;
		size_t	ptr1Size = MALLOC_MEM_T_SIZE - 3;
		printTestName(fd, new_prefix, "test3.1 - allocating memory tiny (1):");
		ptr1 = malloc_allocate(s_malloc, ptr1Size);
		error_nb += assert(fd, NULL != ptr1);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, ptr1 == s_malloc->memory_tiny->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr1Size) == s_malloc->memory_tiny->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr4;
		size_t	ptr4Size = ptr1Size + 1;
		printTestName(fd, new_prefix, "test3.1 - reAllocating memory (1):");
		ptr4 = malloc_reallocate(s_malloc, ptr1, ptr4Size);
		error_nb += assert(fd, NULL != ptr4);
		error_nb += assert(fd, ptr1 == ptr4);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, ptr4 == s_malloc->memory_tiny->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr4Size) == s_malloc->memory_tiny->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test3.1 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test3.1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestName(fd, new_prefix, "test3.2 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr2;
		size_t	ptr2Size = MALLOC_MEM_S_SIZE - 15;
		printTestName(fd, new_prefix, "test3.2 - allocating memory small:");
		ptr2 = malloc_allocate(s_malloc, ptr2Size);
		error_nb += assert(fd, NULL != ptr2);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, ptr2 == s_malloc->memory_small->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr2Size) == s_malloc->memory_small->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr5;
		size_t	ptr5Size = ptr2Size + 1;
		printTestName(fd, new_prefix, "test3.2 - reAllocating memory:");
		ptr5 = malloc_reallocate(s_malloc, ptr2, ptr5Size);
		error_nb += assert(fd, NULL != ptr5);
		error_nb += assert(fd, ptr2 == ptr5);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, ptr5 == s_malloc->memory_small->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr5Size) == s_malloc->memory_small->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test3.2 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test3.2 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestName(fd, new_prefix, "test3.3 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr3;
		size_t	ptr3Size = MALLOC_MEM_S_SIZE + 34;
		printTestName(fd, new_prefix, "test3.3 - allocating memory large:");
		ptr3 = malloc_allocate(s_malloc, ptr3Size);
		error_nb += assert(fd, NULL != ptr3);
		error_nb += assert(fd, NULL != s_malloc->memory_large);
		error_nb += assert(fd, ptr3 == s_malloc->memory_large->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr3Size) == s_malloc->memory_large->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr6;
		size_t	ptr6Size = round_to_page_size_multiple(ptr3Size) + 1;
		printTestName(fd, new_prefix, "test3.3 - reAllocating memory:");
		ptr6 = malloc_reallocate(s_malloc, ptr3, ptr6Size);
		error_nb += assert(fd, NULL != ptr6);
		error_nb += assert(fd, ptr3 != ptr6);
		error_nb += assert(fd, NULL != s_malloc->memory_large);
		error_nb += assert(fd, NULL != s_malloc->memory_large->next);
		error_nb += assert(fd, ptr6 == s_malloc->memory_large->next->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr6Size) == s_malloc->memory_large->next->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test3.3 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test3.3 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	printTestNameEndl(fd, new_prefix, "test4 - Reallocations of each available size, cross size.");
	{
		printTestName(fd, new_prefix, "test4 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr1;
		size_t	ptr1Size = MALLOC_MEM_T_SIZE - 3;
		printTestName(fd, new_prefix, "test4 - allocating memory tiny (1):");
		ptr1 = malloc_allocate(s_malloc, ptr1Size);
		error_nb += assert(fd, NULL != ptr1);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, ptr1 == s_malloc->memory_tiny->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr1Size) == s_malloc->memory_tiny->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr2;
		size_t	ptr2Size = MALLOC_MEM_S_SIZE - 23;
		printTestName(fd, new_prefix, "test4 - reAllocating memory (1) from tiny to small:");
		ptr2 = malloc_reallocate(s_malloc, ptr1, ptr2Size);
		error_nb += assert(fd, NULL != ptr2);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny->lst_data);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, ptr2 == s_malloc->memory_small->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr2Size) == s_malloc->memory_small->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr3;
		size_t	ptr3Size = MALLOC_MEM_S_SIZE + 23;
		printTestName(fd, new_prefix, "test4 - reAllocating memory (1) from small to large:");
		ptr3 = malloc_reallocate(s_malloc, ptr2, ptr3Size);
		error_nb += assert(fd, NULL != ptr3);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_small->lst_data);
		error_nb += assert(fd, NULL != s_malloc->memory_large);
		error_nb += assert(fd, ptr3 == s_malloc->memory_large->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr3Size) == s_malloc->memory_large->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr4;
		size_t	ptr4Size = MALLOC_MEM_S_SIZE - 5;
		printTestName(fd, new_prefix, "test4 - reAllocating memory (1) from large to small:");
		ptr4 = malloc_reallocate(s_malloc, ptr3, ptr4Size);
		error_nb += assert(fd, NULL != ptr4);
		error_nb += assert(fd, NULL != s_malloc->memory_large);
		error_nb += assert(fd, NULL == s_malloc->memory_large->lst_data);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, ptr4 == s_malloc->memory_small->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr4Size) == s_malloc->memory_small->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr5;
		size_t	ptr5Size = MALLOC_MEM_T_SIZE - 2;
		printTestName(fd, new_prefix, "test4 - reAllocating memory (1) from small to tiny:");
		ptr5 = malloc_reallocate(s_malloc, ptr4, ptr5Size);
		error_nb += assert(fd, NULL != ptr5);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_small->lst_data);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, ptr5 == s_malloc->memory_tiny->lst_data->ptr);
		error_nb += assert(fd, round_to_mod16(ptr5Size) == s_malloc->memory_tiny->lst_data->size);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test4 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test4 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	printTestNameEndl(fd, new_prefix, "test5 - Reallocations specific.");
	{
		printTestName(fd, new_prefix, "test5.1 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr6;
		int		invalidPointer = 4;
		size_t	ptr6Size = MALLOC_MEM_T_SIZE - 2;
		printTestName(fd, new_prefix, "test5.1 - reAllocating invalid pointer:");
		ptr6 = malloc_reallocate(s_malloc, &invalidPointer, ptr6Size);
		error_nb += assert(fd, NULL == ptr6);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assertEndln(fd, 0 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test5.1 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test5.1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestName(fd, new_prefix, "test5.2 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr7;
		size_t	ptr7Size = MALLOC_MEM_T_SIZE - 2;
		printTestName(fd, new_prefix, "test5.2 - reAllocating NULL pointer:");
		ptr7 = malloc_reallocate(s_malloc, NULL, ptr7Size);
		error_nb += assert(fd, NULL != ptr7);
		error_nb += assert(fd, NULL != s_malloc->memory_tiny);
		error_nb += assert(fd, ptr7 == s_malloc->memory_tiny->lst_data->ptr);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test5.2 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test5.2 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestName(fd, new_prefix, "test5.3 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr;
		size_t	ptrSize = MALLOC_MEM_S_SIZE - 10;
		printTestName(fd, new_prefix, "test5.3 - allocating  pointer:");
		ptr = malloc_allocate(s_malloc, ptrSize);
		error_nb += assert(fd, NULL != ptr);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		void	*ptr2;
		size_t	ptr2Size = MALLOC_MEM_S_SIZE - 2;
		printTestName(fd, new_prefix, "test5.3 - reAllocating bad address from previously allocated pointer:");
		ptr2 = malloc_reallocate(s_malloc, (void *)ptr + 2, ptr2Size);
		error_nb += assert(fd, NULL == ptr2);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test5.3 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test5.3 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	printTestNameEndl(fd, new_prefix, "test6 - malloc_release specific.");
	{
		printTestName(fd, new_prefix, "test6.1 - instanciating malloc: ");
		t_malloc	*s_malloc;
		s_malloc = malloc_new();
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL == s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assert(fd, 0 == s_malloc->allocation_tracker);
		error_nb += assertEndln(fd, s_malloc != NULL);

		void	*ptr;
		size_t	ptrSize = MALLOC_MEM_S_SIZE - 10;
		printTestName(fd, new_prefix, "test6.1 - allocating  pointer:");
		ptr = malloc_allocate(s_malloc, ptrSize);
		error_nb += assert(fd, NULL != ptr);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestName(fd, new_prefix, "test6.1 - release bad address from previously allocated pointer:");
		malloc_release(s_malloc, (void *)ptr + 2);
		error_nb += assert(fd, NULL != ptr);
		error_nb += assert(fd, NULL == s_malloc->memory_tiny);
		error_nb += assert(fd, NULL != s_malloc->memory_small);
		error_nb += assert(fd, NULL == s_malloc->memory_large);
		error_nb += assertEndln(fd, 1 == s_malloc->allocation_tracker);

		printTestNameEndl(fd, new_prefix, "test6.1 - deleting malloc: OK");
		malloc_delete(s_malloc);
		
		printTestName(fd, new_prefix, "test6.1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	return (error_nb);
}