# include "unitTestFramework.h"
#include <string.h>
#include <stdio.h>

// Terminal colour
//

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

void	printNormal(int fd){
	lib_putstr_fd(KNRM, fd);
}
void	printRed(int fd){
	lib_putstr_fd(KRED, fd);
}
void	printGreen(int fd){
	lib_putstr_fd(KGRN, fd);
}
void	printYellow(int fd){
	lib_putstr_fd(KYEL, fd);
}
void	printBlue(int fd){
	lib_putstr_fd(KBLU, fd);
}
void	printMagenta(int fd){
	lib_putstr_fd(KMAG, fd);
}
void	printCyan(int fd){
	lib_putstr_fd(KCYN, fd);
}

void	printWhite(int fd){
	lib_putstr_fd(KWHT, fd);
}

// Testing functions
//

void	printTestName(int fd, char *prefix, char *testName) {
	lib_putstr_fd(prefix, fd);lib_putstr_fd(testName, fd);
	lib_putendl_fd(testName, FD_DEBUG);
}

void	printTestNameEndl(int fd, char *prefix, char *testName) {
	printTestName(fd, prefix, testName);
	write(fd, "\n", 1);
}

// return 1 if an error is detected.
int	assertEndln(int fd, int boolean) {
	int didDetectError = assert(fd, boolean);
	write(fd, "\n", 1);
	return (didDetectError);
}

// return 1 if an error is detected.
int	assert(int fd, int boolean) {
	if (!boolean) {
		printRed(fd);
		lib_putstr_fd("KO ", fd);
		printNormal(fd);
		return (1);
	}
	else {
		printGreen(fd);
		lib_putstr_fd("OK ", fd);
		printNormal(fd);
		return (0);
	}
}

void fatal(int fd, char	*msg) {
	lib_putendl_fd("", fd);
	lib_putstr_fd("Fatal: ", fd);
	lib_putendl_fd(msg, fd);
	lib_putendl_fd("Aborting all tests", fd);
	_exit(1);
}

// variable:
// _________ int test(int, char *) => numberOfError test(int fd, char *txtPrefix)
int	runTest(int fd, char *txtPrefix, char *testName, TestSuit test) {
	int error_nb;
	char *new_txtPrefix = txtPrefix;

	printTestNameEndl(fd, new_txtPrefix, testName);

	error_nb = test(fd, new_txtPrefix);

	lib_putstr_fd(new_txtPrefix, fd);
	if (error_nb == 0) {
		printGreen(fd);
		lib_putendl_fd("OK", fd);
		printNormal(fd);
	}
	else {
		printRed(fd);
		lib_putstr_fd("Error(s): ", fd);lib_putnbr_fd(error_nb, fd);lib_putendl_fd(".", fd);
		printNormal(fd);
	}
	return (error_nb != 0);
}

/*
void	runTestsSuits(int fd, TestSuits testSuits){
	int		error_nb = 0;
	char	*prefix = "";

	lib_putendl_fd("Starting tests suits.", fd);
	for (int i = 0; testSuits[i] != NULL; i++){
		error_nb += runTest(fd, prefix, "defaultName", testSuits[i]);
	}
	lib_putstr_fd("Tests suits containing (an) error(s): ", fd);lib_putnbr_fd(error_nb, fd);lib_putendl_fd("", fd);
}*/