#include "tests.h"
#include "s_data.h"
#include "s_data_memory_handler.h"
#include <string.h>

int		t_data_memoryHanlder_test(int fd, char *prefix) {
	//preparing tests
	int error_nb = 0;
	char *new_prefix = prefix;
	allocation_counter_set(0);

	//tests
	{
		printTestName(fd, new_prefix, "test1 - Testing one memory page.\n");
		int					NumberOfTData = 5;
		t_data_memory_handler *dmh;

		printTestName(fd, new_prefix, "test1 - creating new dmh: ");
		dmh = t_data_memory_handler_new(NumberOfTData);
		assert(fd, dmh != NULL);
		assert(fd, dmh->number_of_t_data == NumberOfTData);
		assert(fd, dmh->occupied_spots != NULL);
		assert(fd, dmh->occupied_spots[0] == NULL);
		assertEndln(fd, dmh->next == NULL);

		printTestName(fd, new_prefix, "test1 - requesting allocation (1): ");
		t_data	*data1 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[0] == data1);

		printTestName(fd, new_prefix, "test1 - release allocation (1): ");
		release_t_data(dmh, data1);
		assertEndln(fd, dmh->occupied_spots[0] == NULL);

		printTestName(fd, new_prefix, "test1 - requesting allocation (2): ");
		t_data	*data2 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[0] == (void *)data2);
		
		printTestName(fd, new_prefix, "test1 - requesting allocation (3): ");
		t_data	*data3 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[1] == (void *)data3);
		
		printTestName(fd, new_prefix, "test1 - requesting allocation (4): ");
		t_data	*data4 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[2] == (void *)data4);
		
		printTestName(fd, new_prefix, "test1 - requesting allocation (5): ");
		t_data	*data5 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[3] == (void *)data5);

		printTestName(fd, new_prefix, "test1 - release allocation (4): ");
		release_t_data(dmh, data4);
		assertEndln(fd, dmh->occupied_spots[2] == NULL);

		printTestName(fd, new_prefix, "test1 - checking allocation: registered order ");
		assert(fd, dmh->occupied_spots[0] == data2);
		assert(fd, dmh->occupied_spots[1] == data3);
		assert(fd, dmh->occupied_spots[2] == NULL);
		assertEndln(fd, dmh->occupied_spots[3] == data5);

		printTestName(fd, new_prefix, "test1 - clearing dmh: OK\n");
		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestName(fd, new_prefix, "test2 - Testing multiple memory page.\n");
		int					NumberOfTData = 2;
		t_data_memory_handler *dmh;

		printTestName(fd, new_prefix, "test2 - creating new dmh: ");
		dmh = t_data_memory_handler_new(NumberOfTData);
		assert(fd, dmh != NULL);
		assert(fd, dmh->number_of_t_data == NumberOfTData);
		assert(fd, dmh->occupied_spots != NULL);
		assert(fd, dmh->occupied_spots[0] == NULL);
		assertEndln(fd, dmh->next == NULL);

		printTestName(fd, new_prefix, "test2 - requesting allocation (1): ");
		t_data	*data1 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[0] == data1);

		printTestName(fd, new_prefix, "test2 - release allocation (1): ");
		release_t_data(dmh, data1);
		assertEndln(fd, dmh->occupied_spots[0] == NULL);

		printTestName(fd, new_prefix, "test2 - requesting allocation (2): ");
		t_data	*data2 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[0] == (void *)data2);
		
		printTestName(fd, new_prefix, "test2 - requesting allocation (3): ");
		t_data	*data3 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->occupied_spots[1] == (void *)data3);
		
		printTestName(fd, new_prefix, "test2 - requesting allocation (4): ");
		t_data	*data4 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->next->occupied_spots[0] == (void *)data4);
		
		printTestName(fd, new_prefix, "test2 - requesting allocation (5): ");
		t_data	*data5 = allocate_new_t_data(dmh, NULL, 0);
		assertEndln(fd, dmh->next->occupied_spots[1] == (void *)data5);

		printTestName(fd, new_prefix, "test2 - release allocation (4): ");
		release_t_data(dmh, data4);
		assertEndln(fd, dmh->next->occupied_spots[0] == NULL);

		printTestName(fd, new_prefix, "test2 - checking allocation: registered order ");
		assert(fd, dmh->occupied_spots[0] == data2);
		assert(fd, dmh->occupied_spots[1] == data3);
		assert(fd, dmh->next->occupied_spots[0] == NULL);
		assertEndln(fd, dmh->next->occupied_spots[1] == data5);


		printTestNameEndl(fd, new_prefix, "test2 - clearing dmh: OK");
		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test2 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	return (error_nb);
}