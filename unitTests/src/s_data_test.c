#include "tests.h"
#include "s_data.h"
#include <string.h>

int		t_data_test(int fd, char *prefix) {
	//preparing tests
	int error_nb = 0;
	char *new_prefix = prefix;
	allocation_counter_set(0);

	//tests
	{
		printTestName(fd, new_prefix, "test1 - creating new element: ");
		//char * which will be used:
		char *datapass = "passphrase";

		t_data	*lst_dataHead;
		lst_dataHead = t_data_new(datapass, 2);//pa
		error_nb += assertEndln(fd, NULL != lst_dataHead);

		printTestName(fd, new_prefix, "test1 - registering new element: ");
		t_data	*data_new;

		data_new = t_data_new(datapass + 3, 3);//sph
		lst_dataHead = t_data_register_new_data(lst_dataHead, data_new);
		error_nb += assert(fd, NULL != lst_dataHead);

		data_new = t_data_new(datapass + 2, 1);//s
		lst_dataHead = t_data_register_new_data(lst_dataHead, data_new);
		error_nb += assert(fd, NULL != lst_dataHead);

		data_new = t_data_new(datapass + 8, 3);//ras
		lst_dataHead = t_data_register_new_data(lst_dataHead, data_new);
		error_nb += assert(fd, NULL != lst_dataHead);

		error_nb += assertEndln(fd, !(((char *)(lst_dataHead->ptr))[0] != 'p' || ((char *)(lst_dataHead->ptr))[1] != 'a'
			|| ((char *)(lst_dataHead->next->ptr))[0] != 's'
			|| ((char *)(lst_dataHead->next->next->ptr))[0] != 's' || ((char *)(lst_dataHead->next->next->ptr))[1] != 'p' || ((char *)(lst_dataHead->next->next->ptr))[2] != 'h'
			|| ((char *)(lst_dataHead->next->next->next->ptr))[0] != 's' || ((char *)(lst_dataHead->next->next->next->ptr))[1] != 'e'));

		printTestName(fd, new_prefix, "test1 - deleting an element: ");
		t_data_delete(lst_dataHead->next, lst_dataHead->next->next);

		error_nb += assertEndln(fd, !(((char *)(lst_dataHead->ptr))[0] != 'p' || ((char *)(lst_dataHead->ptr))[1] != 'a'
			|| ((char *)(lst_dataHead->next->ptr))[0] != 's'
			|| ((char *)(lst_dataHead->next->next->ptr))[0] != 's' || ((char *)(lst_dataHead->next->next->ptr))[1] != 'e'));
		
		printTestNameEndl(fd, new_prefix, "test1 - deleting the lst_data: OK");
		t_data_clear(lst_dataHead);
		
		printTestName(fd, new_prefix, "test1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestName(fd, new_prefix, "test2 - registering new element in empty list: ");
		t_data	*lst_dataHead;
		t_data	*data_new;
		data_new = t_data_new(NULL, 3);
		lst_dataHead = t_data_register_new_data(NULL, data_new);
		error_nb += assertEndln(fd, NULL != lst_dataHead);

		printTestNameEndl(fd, new_prefix, "test2 - clearing list: OK");
		t_data_clear(lst_dataHead);
		
		printTestName(fd, new_prefix, "test2 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	return (error_nb);
}