#include "tests.h"
#include "memory_handler.h"
#include <string.h>

int		error_handler_set(int error_code);
int		memory_handler_test(int fd, char *prefix) {
	//preparing tests
	int error_nb = 0;
	char *new_prefix = prefix;
	allocation_counter_set(0);

	//tests
	{
		int allocationSize = 4;

		printTestName(fd, new_prefix, "test1 - claiming new memory: ");
		void	*allocatedSpace = mmap_handler(allocationSize);
		error_nb += assertEndln(fd, NULL != allocatedSpace);

		printTestName(fd, new_prefix, "test1 - releasing memory: ");
		error_nb += assertEndln(fd, !munmap_handler(allocatedSpace, allocationSize));

		printTestName(fd, new_prefix, "test1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	return (error_nb);
}
