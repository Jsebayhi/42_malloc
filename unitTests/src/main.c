#include "tests.h"
#include "libft.h"
#include "malloc.h"

int main (){
	int	fd = 1;
	int		error_nb = 0;
	char	*prefix = "\t";

	error_nb += runTest(fd, prefix, "memory_handler tests", &memory_handler_test);
	error_nb += runTest(fd, prefix, "s_data_test", &t_data_test);
	error_nb += runTest(fd, prefix, "t_data_memoryHanlder tests", &t_data_memoryHanlder_test);
	error_nb += runTest(fd, prefix, "t_heap test", &t_heap_test);
	error_nb += runTest(fd, prefix, "t_malloc test", &t_malloc_test);
	error_nb += runTest(fd, prefix, "malloc test", &malloc_test);
	lib_putstr_fd("Tests suits containing (an) error(s): ", fd);lib_putnbr_fd(error_nb, fd);lib_putendl_fd("", fd);
	return (0);
}

/*
int main (){

	int	fd = 1;
	int	testSuitsSize = 6;
	TestSuits testSuits[testSuitsSize];

	testSuits[0] = &mmap_handler_test;
	testSuits[1] = &t_page_test;
	testSuits[2] = &t_data_test;
	testSuits[3] = &t_data_memoryHanlder_test;
	testSuits[testSuitsSize - 1] = NULL;

	runTestsSuits(fd, testSuits);
	return (0);
}
*/