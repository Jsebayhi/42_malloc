#include "tests.h"
#include "s_heap.h"
#include <string.h>

int		t_heap_test(int fd, char *prefix) {
	//preparing tests
	int error_nb = 0;
	char *new_prefix = prefix;
	allocation_counter_set(0);

	//tests
	{
		printTestNameEndl(fd, new_prefix, "test1 - Creation and destruction of a heap list.");
		printTestName(fd, new_prefix, "test1 - creating new heap (1): ");
		t_data_memory_handler	*dmh = t_data_memory_handler_new(DMH_SIZE);
		t_heap	*heap1;

		heap1 = t_heap_new(10, dmh);
		error_nb += assert(fd, NULL != heap1);
		error_nb += assert(fd, 10 == heap1->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap1->next);
		error_nb += assert(fd, NULL == heap1->memory_page_head);
		error_nb += assert(fd, 0 == heap1->memory_page_size);
		error_nb += assert(fd, NULL == heap1->lst_data);
		error_nb += assert(fd, NULL != heap1->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap1->next);

		printTestName(fd, new_prefix, "test1 - creating new heap (2): ");
		t_heap	*heap2;

		heap2 = t_heap_new(10, dmh);
		error_nb += assert(fd, NULL != heap2);
		error_nb += assert(fd, 10 == heap2->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap2->next);
		error_nb += assert(fd, NULL == heap2->memory_page_head);
		error_nb += assert(fd, 0 == heap2->memory_page_size);
		error_nb += assert(fd, NULL == heap2->lst_data);
		error_nb += assert(fd, NULL != heap2->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap2->next);

		printTestName(fd, new_prefix, "test1 - linking heap (2) to heap (1): ");
		heap1->next = heap2;
		error_nb += assertEndln(fd, heap2 == heap1->next);

		printTestNameEndl(fd, new_prefix, "test1 - deleting the heap: OK");
		t_heap_list_delete(heap1);

		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test1 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());

		}
	{
		printTestNameEndl(fd, new_prefix, "test2 - Allocation and release of memory in single heap.");
		t_data_memory_handler	*dmh = t_data_memory_handler_new(DMH_SIZE);
		t_heap	*heap;
		size_t	memory_page_size = 100;

		printTestName(fd, new_prefix, "test2 - creating new heap: ");
		heap = t_heap_new(memory_page_size, dmh);
		error_nb += assert(fd, NULL != heap);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, NULL == heap->memory_page_head);
		error_nb += assert(fd, 0 == heap->memory_page_size);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assert(fd, NULL == heap->lst_data);
		error_nb += assert(fd, NULL != heap->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test2 - requesting new allocation (1): ");
		void	*allocation1;
		size_t	allocation1Size = 10;
		allocation1 = t_heap_allocate(heap, allocation1Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation1 == heap->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(memory_page_size) == heap->memory_page_size);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->ptr == allocation1);
		error_nb += assert(fd, heap->lst_data->size == allocation1Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test2 - requesting new allocation (2): ");
		void	*allocation2;
		size_t	allocation2Size = 22;
		allocation2 = t_heap_allocate(heap, allocation2Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation1 + allocation1Size == allocation2);
		error_nb += assert(fd, 2 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->ptr == allocation2);
		error_nb += assert(fd, heap->lst_data->next->size == allocation2Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test2 - requesting new allocation (3): ");
		void	*allocation3;
		size_t	allocation3Size = 5;
		allocation3 = t_heap_allocate(heap, allocation3Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation2 + allocation2Size == allocation3);
		error_nb += assert(fd, 3 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->next->ptr == allocation3);
		error_nb += assert(fd, heap->lst_data->next->next->size == allocation3Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test2 - requesting release of invalid allocation: ");
		t_heap_list_release(heap, NULL);
		error_nb += assertEndln(fd, S_HEAP__FIND_PTR__NOT_FOUND == error_handler_get());

		printTestName(fd, new_prefix, "test2 - requesting release of allocation (2): ");
		t_heap_list_release(heap, allocation2);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, 2 == heap->allocation_counter);
		error_nb += assert(fd, NULL != heap->memory_page_head);
		error_nb += assert(fd, NULL != heap->lst_data);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test2 - requesting release of allocation (3): ");
		t_heap_list_release(heap, allocation3);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assert(fd, NULL != heap->memory_page_head);
		error_nb += assert(fd, NULL != heap->lst_data);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test2 - requesting release of allocation (1): ");
		t_heap_list_release(heap, allocation1);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assert(fd, NULL != heap->memory_page_head);
		error_nb += assert(fd, NULL == heap->lst_data);
		error_nb += assertEndln(fd, NULL == heap->next);


		printTestNameEndl(fd, new_prefix, "test2 - deleting the heap: OK");
		t_heap_list_delete(heap);

		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test2 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestNameEndl(fd, new_prefix, "test3 - Allocation and release of memory with unfixed memory page size.");
		t_data_memory_handler	*dmh = t_data_memory_handler_new(DMH_SIZE);
		t_heap	*heap;
		size_t	memory_page_size = T_HEAP__MEMORY_PAGE_SIZE_UNFIXED;

		printTestName(fd, new_prefix, "test3 - creating new heap: ");
		heap = t_heap_new(memory_page_size, dmh);
		error_nb += assert(fd, NULL != heap);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, NULL == heap->memory_page_head);
		error_nb += assert(fd, 0 == heap->memory_page_size);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assert(fd, NULL == heap->lst_data);
		error_nb += assert(fd, NULL != heap->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test3 - requesting new allocation (1): ");
		void	*allocation1;
		size_t	allocation1Size = getpagesize() - 3;
		allocation1 = t_heap_allocate(heap, allocation1Size);
		error_nb += assert(fd, NULL != allocation1);
		error_nb += assert(fd, allocation1 == heap->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(allocation1Size) == heap->memory_page_size);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->ptr == allocation1);
		error_nb += assert(fd, heap->lst_data->size == allocation1Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test3 - requesting new allocation (2): ");
		void	*allocation2;
		size_t	allocation2Size = getpagesize() - 2;
		allocation2 = t_heap_allocate(heap, allocation2Size);
		error_nb += assert(fd, NULL != allocation2);
		error_nb += assert(fd, NULL != heap->next);
		error_nb += assert(fd, round_to_page_size_multiple(allocation2Size) == heap->next->memory_page_size);
		error_nb += assert(fd, 1 == heap->next->allocation_counter);
		error_nb += assert(fd, heap->next->lst_data->ptr == allocation2);
		error_nb += assert(fd, heap->next->lst_data->size == allocation2Size);
		error_nb += assertEndln(fd, NULL == heap->next->next);

		printTestName(fd, new_prefix, "test3 - requesting new allocation (3): ");
		void	*allocation3;
		size_t	allocation3Size = 5;
		allocation3 = t_heap_allocate(heap, allocation3Size);
		error_nb += assert(fd, NULL != allocation3);
		error_nb += assert(fd, NULL != heap->next);
		error_nb += assert(fd, NULL != heap->next->next);
		error_nb += assert(fd, round_to_page_size_multiple(allocation3Size) == heap->next->next->memory_page_size);
		error_nb += assert(fd, 1 == heap->next->next->allocation_counter);
		error_nb += assert(fd, heap->next->next->lst_data->ptr == allocation3);
		error_nb += assert(fd, heap->next->next->lst_data->size == allocation3Size);
		error_nb += assertEndln(fd, NULL == heap->next->next->next);

		printTestName(fd, new_prefix, "test3 - requesting release of allocation (2): ");
		t_heap_list_release(heap, allocation2);
		error_nb += assert(fd, allocation3 == heap->next->lst_data->ptr);
		error_nb += assert(fd, 1 == heap->next->allocation_counter);
		error_nb += assertEndln(fd, NULL == heap->next->next);

		printTestName(fd, new_prefix, "test3 - requesting release of allocation (3): ");
		t_heap_list_release(heap, allocation3);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test3 - requesting release of allocation (1): ");
		t_heap_list_release(heap, allocation1);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assertEndln(fd, NULL == heap->lst_data);

		printTestNameEndl(fd, new_prefix, "test3 - deleting the heap: OK");
		t_heap_list_delete(heap);

		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test3 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestNameEndl(fd, new_prefix, "test4 - reallocation in single heap.");
		t_data_memory_handler	*dmh = t_data_memory_handler_new(DMH_SIZE);
		t_heap	*heap;
		size_t	memory_page_size = 100;

		printTestName(fd, new_prefix, "test4 - creating new heap: ");
		heap = t_heap_new(memory_page_size, dmh);
		error_nb += assert(fd, NULL != heap);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, NULL == heap->memory_page_head);
		error_nb += assert(fd, 0 == heap->memory_page_size);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assert(fd, NULL == heap->lst_data);
		error_nb += assert(fd, NULL != heap->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting new allocation (1): ");
		void	*allocation1;
		size_t	allocation1Size = 10;
		allocation1 = t_heap_allocate(heap, allocation1Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation1 == heap->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(memory_page_size) == heap->memory_page_size);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->ptr == allocation1);
		error_nb += assert(fd, heap->lst_data->size == allocation1Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting new allocation (2): ");
		void	*allocation2;
		size_t	allocation2Size = 10;
		allocation2 = t_heap_allocate(heap, allocation2Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation1 + allocation1Size == allocation2);
		error_nb += assert(fd, 2 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->ptr == allocation2);
		error_nb += assert(fd, heap->lst_data->next->size == allocation2Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting new allocation (3): ");
		void	*allocation3;
		size_t	allocation3Size = 5;
		allocation3 = t_heap_allocate(heap, allocation3Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation2 + allocation2Size == allocation3);
		error_nb += assert(fd, 3 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->next->ptr == allocation3);
		error_nb += assert(fd, heap->lst_data->next->next->size == allocation3Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting new allocation (4): ");
		void	*allocation4;
		size_t	allocation4Size = 15;
		allocation4 = t_heap_allocate(heap, allocation4Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation3 + allocation3Size == allocation4);
		error_nb += assert(fd, 4 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->next->next->ptr == allocation4);
		error_nb += assert(fd, heap->lst_data->next->next->next->size == allocation4Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting release of allocation (3): ");
		t_heap_list_release(heap, allocation3);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, 3 == heap->allocation_counter);
		error_nb += assert(fd, NULL != heap->memory_page_head);
		error_nb += assert(fd, NULL != heap->lst_data);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting reallocation of (2): ");
		allocation2Size = 15;
		allocation2 = t_heap_reallocate(heap, allocation2, allocation2Size);
		error_nb += assert(fd, allocation2 == heap->lst_data->next->ptr);
		error_nb += assert(fd, allocation2Size == heap->lst_data->next->size);
		error_nb += assert(fd, allocation2 == heap->lst_data->ptr + heap->lst_data->size);
		error_nb += assert(fd, allocation4 == heap->lst_data->next->ptr + heap->lst_data->next->size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test4 - requesting reallocation of (2) which will require a new allocation: ");
		allocation2Size = 30;
		allocation2 = t_heap_reallocate(heap, allocation2, allocation2Size);
		error_nb += assert(fd, allocation2 == heap->lst_data->next->next->ptr);
		error_nb += assert(fd, allocation2Size == heap->lst_data->next->next->size);
		error_nb += assert(fd, allocation2 == heap->lst_data->next->ptr + heap->lst_data->next->size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestNameEndl(fd, new_prefix, "test4 - deleting the heap: OK");
		t_heap_list_delete(heap);

		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test4 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestNameEndl(fd, new_prefix, "test5 - reallocation with unfixed memory page size..");
		t_data_memory_handler	*dmh = t_data_memory_handler_new(DMH_SIZE);
		t_heap	*heap;
		size_t	memory_page_size = T_HEAP__MEMORY_PAGE_SIZE_UNFIXED;

		printTestName(fd, new_prefix, "test5 - creating new heap: ");
		heap = t_heap_new(memory_page_size, dmh);
		error_nb += assert(fd, NULL != heap);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, NULL == heap->memory_page_head);
		error_nb += assert(fd, 0 == heap->memory_page_size);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assert(fd, NULL == heap->lst_data);
		error_nb += assert(fd, NULL != heap->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test5 - requesting new allocation (1): ");
		void	*allocation1;
		size_t	allocation1Size = getpagesize() - 10;
		allocation1 = t_heap_allocate(heap, allocation1Size);
		error_nb += assert(fd, NULL != allocation1);
		error_nb += assert(fd, allocation1 == heap->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(allocation1Size) == heap->memory_page_size);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->ptr == allocation1);
		error_nb += assert(fd, heap->lst_data->size == allocation1Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test5 - requesting new allocation (2): ");
		void	*allocation2;
		size_t	allocation2Size = getpagesize() - 10;
		allocation2 = t_heap_allocate(heap, allocation2Size);
		error_nb += assert(fd, NULL != allocation2);
		error_nb += assert(fd, allocation2 == heap->next->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(allocation2Size) == heap->next->memory_page_size);
		error_nb += assert(fd, 1 == heap->next->allocation_counter);
		error_nb += assert(fd, heap->next->lst_data->ptr == allocation2);
		error_nb += assert(fd, heap->next->lst_data->size == allocation2Size);
		error_nb += assertEndln(fd, NULL == heap->next->next);

		printTestName(fd, new_prefix, "test5 - requesting new allocation (3): ");
		void	*allocation3;
		size_t	allocation3Size = getpagesize() - 15;
		allocation3 = t_heap_allocate(heap, allocation3Size);
		error_nb += assert(fd, NULL != allocation3);
		error_nb += assert(fd, allocation3 == heap->next->next->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(allocation3Size) == heap->next->next->memory_page_size);
		error_nb += assert(fd, 1 == heap->next->next->allocation_counter);
		error_nb += assert(fd, heap->next->next->lst_data->ptr == allocation3);
		error_nb += assert(fd, heap->next->next->lst_data->size == allocation3Size);
		error_nb += assertEndln(fd, NULL == heap->next->next->next);

		printTestName(fd, new_prefix, "test5 - requesting new allocation (4): ");
		void	*allocation4;
		size_t	allocation4Size = getpagesize() - 5;
		allocation4 = t_heap_allocate(heap, allocation4Size);
		error_nb += assert(fd, NULL != allocation4);
		error_nb += assert(fd, allocation4 == heap->next->next->next->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(allocation4Size) == heap->next->next->next->memory_page_size);
		error_nb += assert(fd, 1 == heap->next->next->next->allocation_counter);
		error_nb += assert(fd, heap->next->next->next->lst_data->ptr == allocation4);
		error_nb += assert(fd, heap->next->next->next->lst_data->size == allocation4Size);
		error_nb += assertEndln(fd, NULL == heap->next->next->next->next);

		printTestName(fd, new_prefix, "test5 - requesting reallocation of (2): ");
		allocation2Size = getpagesize() + 5;
		allocation2 = t_heap_reallocate(heap, allocation2, allocation2Size);
		error_nb += assert(fd, round_to_page_size_multiple(allocation2Size) == heap->next->next->next->memory_page_size);
		error_nb += assert(fd, allocation2 == heap->next->next->next->memory_page_head);
		error_nb += assert(fd, allocation2 == heap->next->next->next->lst_data->ptr);
		error_nb += assert(fd, allocation2Size == heap->next->next->next->lst_data->size);
		error_nb += assertEndln(fd, 1 == heap->next->next->next->allocation_counter);

		printTestNameEndl(fd, new_prefix, "test5 - deleting the heap: OK");
		t_heap_list_delete(heap);

		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test5 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	{
		printTestNameEndl(fd, new_prefix, "test6 - Checking buffer overflow handling.");
		t_data_memory_handler	*dmh = t_data_memory_handler_new(DMH_SIZE);
		t_heap	*heap;
		size_t	memory_page_size = 100;

		printTestName(fd, new_prefix, "test6 - creating new heap: ");
		heap = t_heap_new(memory_page_size, dmh);
		error_nb += assert(fd, NULL != heap);
		error_nb += assert(fd, memory_page_size == heap->fixed_memory_page_size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, NULL == heap->memory_page_head);
		error_nb += assert(fd, 0 == heap->memory_page_size);
		error_nb += assert(fd, 0 == heap->allocation_counter);
		error_nb += assert(fd, NULL == heap->lst_data);
		error_nb += assert(fd, NULL != heap->data_memory_handler);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test6 - requesting new allocation (1): ");
		void	*allocation1;
		size_t	allocation1Size = 10;
		allocation1 = t_heap_allocate(heap, allocation1Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation1 == heap->memory_page_head);
		error_nb += assert(fd, round_to_page_size_multiple(memory_page_size) == heap->memory_page_size);
		error_nb += assert(fd, 1 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->ptr == allocation1);
		error_nb += assert(fd, heap->lst_data->size == allocation1Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test6 - requesting new allocation (2): ");
		void	*allocation2;
		size_t	allocation2Size = 3;
		allocation2 = t_heap_allocate(heap, allocation2Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation1 + allocation1Size == allocation2);
		error_nb += assert(fd, 2 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->ptr == allocation2);
		error_nb += assert(fd, heap->lst_data->next->size == allocation2Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test6 - requesting new allocation (3): ");
		void	*allocation3;
		size_t	allocation3Size = 5;
		allocation3 = t_heap_allocate(heap, allocation3Size);
		error_nb += assert(fd, NULL == heap->next);
		error_nb += assert(fd, allocation2 + allocation2Size == allocation3);
		error_nb += assert(fd, 3 == heap->allocation_counter);
		error_nb += assert(fd, heap->lst_data->next->next->ptr == allocation3);
		error_nb += assert(fd, heap->lst_data->next->next->size == allocation3Size);
		error_nb += assertEndln(fd, NULL == heap->next);

		printTestName(fd, new_prefix, "test6 - Making a buffer overflow: ");
		strcpy(allocation1, "ttttttttt");
		strcpy(allocation2, "uu");
		strcpy(allocation3, "vvvv");

		error_nb += assert(fd, 0 == strcmp("ttttttttt", allocation1));
		error_nb += assert(fd, 0 == strcmp("uu", allocation2));
		error_nb += assert(fd, 0 == strcmp("vvvv", allocation3));

		strcpy(allocation2, "aaaaa");

		error_nb += assert(fd, 0 == strcmp("ttttttttt", allocation1));
		error_nb += assert(fd, 0 == strcmp("aaaaa", allocation2));
		error_nb += assertEndln(fd, 0 == strcmp("aa", allocation3));

		printTestNameEndl(fd, new_prefix, "test6 - deleting the heap: OK");
		t_heap_list_delete(heap);

		t_data_memory_handler_clear(dmh);
		
		printTestName(fd, new_prefix, "test6 - checking allocation counter: ");
		error_nb += assertEndln(fd, 0 == allocation_counter_get());
	}
	return (error_nb);
}