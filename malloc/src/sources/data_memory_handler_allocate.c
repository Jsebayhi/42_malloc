/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_memory_handler_allocate.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 16:19:43 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data_memory_handler.h"

/*
** check if there is space to register a new t_data and append a new
** s_data_memory_handler if needs be.
*/

t_data	*allocate_new_t_data(t_data_memory_handler *dmh, void *ptr, size_t size)
{
	t_data	*data;
	int		offset;

	offset = 0;
	while (offset < dmh->number_of_t_data)
	{
		if (dmh->occupied_spots[offset] == NULL)
		{
			data = (t_data *)(dmh->memory_head + sizeof(t_data) * offset);
			dmh->occupied_spots[offset] = data;
			return (t_data_initializer(data, ptr, size));
		}
		offset++;
	}
	if (!dmh->next)
		dmh->next = t_data_memory_handler_new(dmh->number_of_t_data);
	return (allocate_new_t_data(dmh->next, ptr, size));
}
