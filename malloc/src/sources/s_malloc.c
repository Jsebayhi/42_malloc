/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_malloc.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 13:07:32 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_handler.h"
#include "errors.h"
#include "libft.h"
#include "s_malloc.h"

/*
** Constructor
** -----------
*/

t_malloc	*malloc_new(void)
{
	t_malloc	*malloc;

	if (!(malloc = mmap_handler(sizeof(t_malloc))))
		return (NULL);
	return (malloc_initialize(malloc));
}

t_malloc	*malloc_initialize(t_malloc *malloc)
{
	malloc->data_memory_handler = t_data_memory_handler_new(DMH_SIZE);
	if (!malloc->data_memory_handler)
	{
		malloc_delete(malloc);
		return (NULL);
	}
	malloc->allocation_tracker = 0;
	malloc->memory_tiny = NULL;
	malloc->memory_small = NULL;
	malloc->memory_large = NULL;
	malloc->has_been_initialized = 0xdeadcafe;
	return (malloc);
}

/*
** Destructor
** ----------
*/

void		malloc_delete(t_malloc *malloc)
{
	if (malloc->memory_tiny)
		t_heap_list_delete(malloc->memory_tiny);
	if (malloc->memory_small)
		t_heap_list_delete(malloc->memory_small);
	if (malloc->memory_large)
		t_heap_list_delete(malloc->memory_large);
	if (malloc->data_memory_handler)
		t_data_memory_handler_clear(malloc->data_memory_handler);
	if (munmap_handler(malloc, sizeof(t_malloc)))
		error_handler_set(S_MALLOC__UNABLE_TO_RELEASE_MEMORY);
}

/*
** Show info
** ---------
*/

void		malloc_show_info(t_malloc *malloc, int fd)
{
	lib_putstr_fd("TINY: ", fd);
	if (malloc->memory_tiny)
		t_heap_show_info(malloc->memory_tiny, fd);
	else
		lib_putendl_fd("", fd);
	lib_putstr_fd("SMALL: ", fd);
	if (malloc->memory_small)
		t_heap_show_info(malloc->memory_small, fd);
	else
		lib_putendl_fd("", fd);
	lib_putstr_fd("HIGH: ", fd);
	if (malloc->memory_large)
		t_heap_show_info(malloc->memory_large, fd);
	else
		lib_putendl_fd("", fd);
}

/*
** Allocation tracking
** -------------------
*/

void		allocation_tracker_add(t_malloc *malloc, int add)
{
	malloc->allocation_tracker += add;
}
