/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_data_register.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 14:44:05 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data.h"

/*
** Insert the new data where it belongs based on the pointer value.
** Return: the head of the list.
*/

t_data	*t_data_register_new_data(t_data *lst_data_head, t_data *data_new)
{
	t_data *offset;

	if (!lst_data_head)
		return (data_new);
	if (lst_data_head->ptr > data_new->ptr)
		return (t_data_insert_before(lst_data_head, data_new));
	offset = lst_data_head;
	while (offset && offset->next)
	{
		if (offset->next->ptr > data_new->ptr)
		{
			t_data_insert_after(offset, data_new);
			return (lst_data_head);
		}
		offset = offset->next;
	}
	if (offset)
		offset->next = data_new;
	return (lst_data_head);
}

t_data	*t_data_insert_before(t_data *data, t_data *data_new)
{
	data_new->next = data;
	return (data);
}

/*
** variable:
** _________ previous_data: data after which to insert the new element.
*/

void	t_data_insert_after(t_data *previous_data, t_data *data_new)
{
	t_data *temporary_pointer;

	temporary_pointer = previous_data->next;
	previous_data->next = data_new;
	data_new->next = temporary_pointer;
}
