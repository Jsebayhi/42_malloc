/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_memory_handler_release.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 16:19:52 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data_memory_handler.h"

/*
** TODO: add deletion of last memory page if no data are registered on it and
** it is not the only one.
*/

void	release_t_data(t_data_memory_handler *dmh, t_data *data_to_release)
{
	size_t	offset;

	offset = 0;
	while (offset < dmh->occupied_spots_length)
	{
		if (dmh->occupied_spots[offset] == (void *)data_to_release)
		{
			dmh->occupied_spots[offset] = NULL;
			return ;
		}
		offset++;
	}
	if (dmh->next)
		return (release_t_data(dmh->next, data_to_release));
	error_handler_set(S_DATA_MEMORY_HANDLER__T_DATA_NOT_FOUND);
}
