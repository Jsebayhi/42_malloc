/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ext_calloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:07:29 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include "libft.h"

void	*calloc(size_t block, size_t size)
{
	void *ptr;

	if (DEBUG)
		lib_putendl_fd("[DEBUG] called calloc", FD_DEBUG);
	ptr = calloc_sub(block, size);
	if (DEBUG && DEBUG_WITH_DETAIL)
	{
		lib_putendl_fd("[DEBUG] state after calloc", FD_DEBUG);
		show_alloc_mem(FD_DEBUG);
	}
	return (ptr);
}

void	*calloc_sub(size_t block, size_t size)
{
	int		rs;
	void	*ptr;

	rs = block * size;
	ptr = malloc_sub(rs);
	lib_bzero(ptr, rs);
	return (ptr);
}
