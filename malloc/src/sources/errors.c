/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 16:21:17 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "errors.h"

/*
** Variable:
** _________ apply: 1 to apply provided code.
*/

int		error_handler(int new_code, int apply)
{
	static int	error_code;

	if (apply == ERROR_HANDLER__APPLY)
	{
		error_code = new_code;
		if (new_code != ERROR_HANDLER__DEFAULT_ERROR)
			debug();
	}
	return (error_code);
}

int		error_handler_get(void)
{
	return (error_handler(0, ERROR_HANDLER__DO_NOT_APPLY_CODE));
}

int		error_handler_set(int error_code)
{
	return (error_handler(error_code, ERROR_HANDLER__APPLY));
}

int		error_handler_reset(void)
{
	return (error_handler(ERROR_HANDLER__DEFAULT_ERROR, ERROR_HANDLER__APPLY));
}

void	debug(void)
{
	if (DEBUG == 1)
	{
		lib_putstr_fd("[DEBUG] error: ", FD_DEBUG);
		lib_putnbr_fd(error_handler_get(), FD_DEBUG);
		lib_putendl_fd("", FD_DEBUG);
	}
}
