/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_heap_allocate.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:52:39 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "errors.h"
#include "memory_handler.h"
#include "s_heap.h"

static void	*t_heap_allocate_element(t_heap *heap, size_t size, void *ptr)
{
	t_data	*data_new;
	t_data	*data_new_head;

	if (!(data_new = allocate_new_t_data(heap->data_memory_handler, ptr, size)))
		return (NULL);
	if (!(data_new_head = t_data_register_new_data(heap->lst_data, data_new)))
	{
		release_t_data(heap->data_memory_handler, data_new);
		return (NULL);
	}
	heap->lst_data = data_new_head;
	t_heap_allocation_add(heap, 1);
	return (ptr);
}

void		*t_heap_allocate(t_heap *heap, size_t size)
{
	void	*ptr;

	if (!heap->memory_page_head && !t_heap_memory_page_initialize(heap, size))
		return (NULL);
	if (!(ptr = t_heap_find_available_block_of_size(heap, size)))
	{
		if (!heap->next && !(heap->next = t_heap_new(\
			heap->fixed_memory_page_size,\
			heap->data_memory_handler)))
		{
			return (NULL);
		}
		return (t_heap_allocate(heap->next, size));
	}
	return (t_heap_allocate_element(heap, size, ptr));
}

/*
** Initialize the page.
** Return NULL if an error occured.
*/

t_heap		*t_heap_memory_page_initialize(t_heap *heap, size_t target_size)
{
	if (heap->fixed_memory_page_size != T_HEAP__MEMORY_PAGE_SIZE_UNFIXED)
	{
		heap->memory_page_size = round_to_page_size_multiple(\
			heap->fixed_memory_page_size);
	}
	else
		heap->memory_page_size = round_to_page_size_multiple(target_size);
	heap->memory_page_head = mmap_handler(heap->memory_page_size);
	if (!heap->memory_page_head)
	{
		error_handler_set(S_HEAP__UNABLE_TO_ALLOCATE_MEMORY);
		return (NULL);
	}
	error_handler_set(ERROR_HANDLER__NO_ERROR);
	return (heap);
}

static int	t_heap_found_available_block_of_size(\
	t_heap *heap,\
	t_data *offset,\
	size_t size)
{
	if (offset->next)
	{
		if (is_block_big_enough(\
			offset->ptr + offset->size,\
			offset->next->ptr,\
			size))
		{
			return (1);
		}
	}
	else if (is_block_big_enough(\
		offset->ptr + offset->size,\
		heap->memory_page_head + heap->memory_page_size,\
		size))
	{
		return (1);
	}
	return (0);
}

void		*t_heap_find_available_block_of_size(t_heap *heap, size_t size)
{
	t_data	*offset;

	if (!heap->lst_data)
		return (heap->memory_page_head);
	offset = heap->lst_data;
	while (offset)
	{
		if (t_heap_found_available_block_of_size(heap, offset, size))
			return (offset->ptr + offset->size);
		offset = offset->next;
	}
	return (NULL);
}
