/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_status.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 13:23:56 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc_status.h"

/*
** code:
** _____ 0: uninitialize
** _____ 1: initialize
** _____ n: return status
**
** return:
** _______ 0: not initialized
** _______ 123456789: initialized
*/

static int	malloc_status(int code)
{
	static int	is_malloc_initialized;

	if (0 == code)
	{
		is_malloc_initialized = 0;
	}
	else if (1 == code)
	{
		is_malloc_initialized = 123456789;
	}
	return (is_malloc_initialized);
}

int			is_malloc_initialized(void)
{
	return (malloc_status(3) == 123456789);
}

void		initialize_malloc_status(void)
{
	malloc_status(0);
}

void		set_malloc_status_initialized(void)
{
	if (is_malloc_initialized())
	{
		error_handler_set(MALLOC_STATUS__ALREADY_INITIALIZED);
	}
	else
	{
		malloc_status(1);
	}
}

void		set_malloc_status_not_initialized(void)
{
	if (!is_malloc_initialized())
	{
		error_handler_set(MALLOC_STATUS__ALREADY_NOT_INITIALIZED);
	}
	else
	{
		malloc_status(0);
	}
}
