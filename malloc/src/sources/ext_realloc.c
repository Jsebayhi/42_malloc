/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ext_realloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 12:57:43 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	*realloc(void *ptr, size_t size)
{
	void	*new_ptr;

	if (DEBUG)
		lib_putendl_fd("[DEBUG] called realloc", FD_DEBUG);
	new_ptr = realloc_sub(ptr, size);
	if (DEBUG && DEBUG_WITH_DETAIL)
	{
		lib_putendl_fd("[DEBUG] state after realloc", FD_DEBUG);
		show_alloc_mem(FD_DEBUG);
	}
	return (new_ptr);
}

void	*realloc_sub(void *ptr, size_t size)
{
	return (malloc_reallocate(get_malloc_struct(), ptr, size));
}
