/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory_handler.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:37:19 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_handler.h"

size_t	round_to_page_size_multiple(size_t size)
{
	size_t	page_size;
	int		rest;

	page_size = getpagesize();
	if (size < page_size)
		return (page_size);
	rest = size % page_size;
	return (size - rest + page_size);
}

void	*mmap_handler(size_t size)
{
	void	*ret;

	ret = mmap(0, size, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
	if (ret == (void*)-1)
		error_handler_set(MMAP__UNABLE_TO_ALLOCATE_MEMORY);
	else
		allocation_counter_add(1);
	lib_bzero(ret, size);
	return (ret);
}

int		munmap_handler(void *addr, size_t length)
{
	int ret;

	ret = 0;
	ret = munmap(addr, length);
	if (ret == -1)
		error_handler_set(MUNMAP__UNABLE_TO_RECLAIM_MEMORY);
	else
		allocation_counter_add(-1);
	return (ret != 0);
}
