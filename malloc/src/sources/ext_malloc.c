/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ext_malloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 15:01:59 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	*malloc(size_t size)
{
	void *ptr;

	if (DEBUG)
	{
		lib_putendl_fd("[DEBUG] called malloc", FD_DEBUG);
	}
	ptr = malloc_sub(size);
	if (DEBUG)
	{
		print_address_endl_fd(ptr, FD_DEBUG);
		if (DEBUG_WITH_DETAIL)
		{
			lib_putendl_fd("[DEBUG] state after malloc", FD_DEBUG);
			show_alloc_mem(FD_DEBUG);
		}
	}
	return (ptr);
}

void	*malloc_sub(size_t size)
{
	return (malloc_allocate(get_malloc_struct(), size));
}
