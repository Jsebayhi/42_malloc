/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_struct.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:09:38 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

/*
** m:
** __ NULL: return the structure
** __ !NULL: save the structure in place of the previous one
*/

static t_malloc	*malloc_struct(t_malloc *m)
{
	static t_malloc	*malloc_struct;

	if (NULL != m)
	{
		malloc_struct = m;
	}
	return (malloc_struct);
}

static void		intialize_malloc_struct(void)
{
	if (DEBUG)
	{
		write(1, "intialize_malloc_struct\n", 24);
	}
	malloc_struct(malloc_new());
	set_malloc_status_initialized();
}

void			uninitialize_malloc_struct(void)
{
	if (DEBUG)
	{
		write(1, "uninitialize_malloc_struct\n", 27);
	}
	malloc_delete(malloc_struct(NULL));
	set_malloc_status_not_initialized();
}

t_malloc		*get_malloc_struct(void)
{
	if (!is_malloc_initialized())
	{
		intialize_malloc_struct();
	}
	return (malloc_struct(NULL));
}
