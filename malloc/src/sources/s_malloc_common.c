/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_malloc_common.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 13:06:12 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_handler.h"
#include "errors.h"
#include "libft.h"
#include "s_malloc.h"

void	*lib_memcpy2(\
	void *dst,\
	const void *src,\
	size_t dst_size,\
	size_t src_size)
{
	if (dst_size < src_size)
		return (lib_memcpy(dst, src, dst_size));
	return (lib_memcpy(dst, src, src_size));
}

void	*lib_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char	*s1;
	unsigned char	*s2;

	s1 = (unsigned char *)src;
	s2 = (unsigned char *)dst;
	if (dst == src || n == 0)
		return (s2);
	while (n--)
		*s2++ = *s1++;
	return ((void *)dst);
}

/*
** LawEnforcer
*/

size_t	round_to_mod16(size_t size)
{
	if (size % 16 != 0)
		return (size + (16 - (size % 16)));
	return (size);
}

int		is_size_tiny(size_t size)
{
	if (size <= MALLOC_MEM_T_SIZE)
		return (1);
	return (0);
}

int		is_size_small(size_t size)
{
	if (MALLOC_MEM_T_SIZE < size && size <= MALLOC_MEM_S_SIZE)
		return (1);
	return (0);
}
