/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_malloc_release.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 12:50:10 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_handler.h"
#include "errors.h"
#include "libft.h"
#include "s_malloc.h"

static int	malloc_try_heap_release(t_heap *heap, void *ptr)
{
	error_handler_reset();
	t_heap_list_release(heap, ptr);
	if (error_handler_get() == ERROR_HANDLER__NO_ERROR)
		return (1);
	return (0);
}

void		malloc_release(t_malloc *malloc, void *ptr)
{
	int done;

	done = 0;
	if (malloc->memory_tiny)
		done = malloc_try_heap_release(malloc->memory_tiny, ptr);
	if (!done && malloc->memory_small)
		done = malloc_try_heap_release(malloc->memory_small, ptr);
	if (!done && malloc->memory_large)
		done = malloc_try_heap_release(malloc->memory_large, ptr);
	if (done)
		allocation_tracker_add(malloc, -1);
}
