/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocation_counter.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 13:32:54 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "errors.h"
#include "memory_handler.h"
#include "libft.h"
#include <sys/mman.h>
#include <sys/types.h>
#include <stdio.h>
#include <errno.h>

/*
** variables:
** __________ new_value: the new value of the counter.
** __________ apply: boolean, != 0 to apply.
*/

static int	allocation_counter(int new_value, int apply)
{
	static int	counter;

	if (apply)
		counter = new_value;
	return (counter);
}

int			allocation_counter_get(void)
{
	int do_not_apply_value;

	do_not_apply_value = 0;
	return (allocation_counter(0, do_not_apply_value));
}

void		allocation_counter_set(int n)
{
	int apply_value;

	apply_value = 1;
	allocation_counter(n, apply_value);
}

void		allocation_counter_show(void)
{
	int counter;

	counter = allocation_counter_get();
	if (SHOW_ALLOCATION_COUNTER == FLAG_SHOW_ALLOCATION_COUNTER)
	{
		lib_putstr_fd("[DEBUG] Number of allocation alive: ", FD_DEBUG);
		lib_putnbr_fd(counter, FD_DEBUG);
		lib_putendl_fd("", FD_DEBUG);
	}
}

int			allocation_counter_add(int add)
{
	int counter;
	int apply_value;

	apply_value = 1;
	counter = allocation_counter(0, !apply_value);
	counter = allocation_counter(counter + add, apply_value);
	allocation_counter_show();
	return (counter);
}
