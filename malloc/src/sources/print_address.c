/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_address.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/06/10 15:20:02 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "standalone_functions.h"
#include "libft.h"

void	print_address_fd(void *ptr, int fd)
{
	char	buffer[print_memory_str_size()];

	print_memory(ptr, buffer);
	lib_putstr_fd(buffer, fd);
}

void	print_address_endl_fd(void *ptr, int fd)
{
	char	buffer[print_memory_str_size()];

	print_memory(ptr, buffer);
	lib_putendl_fd(buffer, fd);
}
