/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_heap_release.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 14:38:39 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "errors.h"
#include "memory_handler.h"
#include "s_heap.h"

void	t_heap_list_release(t_heap *heap_head, void *ptr)
{
	t_heap	*offset;
	t_heap	*tmp;

	error_handler_reset();
	if (t_heap_release(heap_head, ptr))
		return ;
	offset = heap_head;
	while (offset && offset->next)
	{
		if (t_heap_release(offset->next, ptr))
		{
			if (0 == offset->next->allocation_counter)
			{
				tmp = offset->next->next;
				t_heap_delete(offset->next);
				offset->next = tmp;
			}
			return ;
		}
		offset = offset->next;
	}
	error_handler_set(S_HEAP__FIND_PTR__NOT_FOUND);
}

/*
** Return boolean: 0 ptr not found.
*/

int		t_heap_release(t_heap *heap, void *ptr)
{
	t_data	*offset;
	t_data	*tmp;

	if (heap->lst_data && heap->lst_data->ptr == ptr)
	{
		offset = heap->lst_data;
		heap->lst_data = heap->lst_data->next;
		release_t_data(heap->data_memory_handler, offset);
		t_heap_allocation_add(heap, -1);
		return (1);
	}
	offset = heap->lst_data;
	while (offset && offset->next)
	{
		if (offset->next->ptr == ptr)
		{
			tmp = offset->next->next;
			release_t_data(heap->data_memory_handler, offset->next);
			offset->next = tmp;
			t_heap_allocation_add(heap, -1);
			return (1);
		}
		offset = offset->next;
	}
	return (0);
}
