/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_data_find.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 14:45:48 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data.h"

/*
** return:
** _______ The found data.
** _______ NULL if an error occured or no match.
*/

t_data	*t_data_find_ptr(t_data *lst_data_head, void *ptr)
{
	t_data	*offset;

	offset = lst_data_head;
	while (offset)
	{
		if (offset->ptr == ptr)
			return (offset);
		offset = offset->next;
	}
	error_handler_set(S_DATA__FIND_PTR__NOT_FOUND);
	return (NULL);
}
