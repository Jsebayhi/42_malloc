/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ext_free.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 15:02:04 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	free(void *ptr)
{
	if (DEBUG)
	{
		lib_putendl_fd("[DEBUG] called free", FD_DEBUG);
		print_address_endl_fd(ptr, FD_DEBUG);
	}
	free_sub(ptr);
	if (DEBUG && DEBUG_WITH_DETAIL)
	{
		lib_putendl_fd("[DEBUG] state after free", FD_DEBUG);
		show_alloc_mem(FD_DEBUG);
	}
}

void	free_sub(void *ptr)
{
	t_malloc	*m;

	m = get_malloc_struct();
	malloc_release(m, ptr);
	if (0 == m->allocation_tracker)
	{
		uninitialize_malloc_struct();
	}
}
