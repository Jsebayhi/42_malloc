/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ext_show_alloc_mem.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 12:57:48 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void	show_alloc_mem(void)
{
	show_alloc_mem_sub(1);
}

void	show_alloc_mem_sub(int fd)
{
	if (!is_malloc_initialized())
	{
		lib_putendl("TINY:");
		lib_putendl("SMALL:");
		lib_putendl("HIGH:");
	}
	else
	{
		malloc_show_info(get_malloc_struct(), fd);
	}
}
