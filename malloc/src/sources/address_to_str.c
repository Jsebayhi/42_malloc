/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   address_to_str.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:25:31 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static void	adress_str_len(unsigned long int n, size_t *len)
{
	if (n == 0)
	{
		*len = 1;
		return ;
	}
	*len = 0;
	while (n > 0)
	{
		n /= 16;
		*len += 1;
	}
}

static void	init_adress_str(char *str, size_t str_size)
{
	size_t i;

	i = 0;
	str[i++] = '0';
	str[i++] = 'x';
	while (i < str_size - 1)
		str[i++] = '0';
	str[str_size - 1] = 0;
}

/*
** BECAUSE I HATE MACRO EVERYWHERE, THEY ARE LIKE GLOABLS BUT WORSE
** CONST:
**  size_t str_size = 19 => "0x" + nombre en hexa + '\0' => 2 + 16 + 1 = 19
*/

int			print_memory_str_size(void)
{
	return (12);
}

static void	print_memory_sub(unsigned long int nb, size_t str_size, char *str)
{
	unsigned int		i;
	size_t				str_prefix_size;
	size_t				str_null_size;
	size_t				str_ini_offset_size;

	str_prefix_size = 2;
	str_null_size = 1;
	str_ini_offset_size = 1;
	i = str_ini_offset_size;
	while (nb > 0 && str_size >= str_prefix_size + i + str_null_size)
	{
		str[str_size - i - str_null_size] = "0123456789ABCDEF"[nb % 16];
		i++;
		nb /= 16;
	}
}

void		print_memory(void *p, char *str)
{
	unsigned long int	nb;
	size_t				len;
	size_t				str_size;

	str_size = print_memory_str_size();
	nb = (unsigned long int)p;
	init_adress_str(str, str_size);
	adress_str_len(nb, &len);
	print_memory_sub(nb, str_size, str);
}
