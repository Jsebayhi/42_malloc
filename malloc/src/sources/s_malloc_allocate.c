/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_malloc_allocate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 13:19:12 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_handler.h"
#include "errors.h"
#include "libft.h"
#include "s_malloc.h"

static void	*malloc_allocate_tiny(t_malloc *malloc, size_t size)
{
	if (!malloc->memory_tiny)
	{
		malloc->memory_tiny = t_heap_new(\
			MALLOC_MEM_TINY_LGTH,\
			malloc->data_memory_handler);
	}
	if (!malloc->memory_tiny)
		return (NULL);
	return (t_heap_allocate(malloc->memory_tiny, size));
}

static void	*malloc_allocate_small(t_malloc *malloc, size_t size)
{
	if (!malloc->memory_small)
	{
		malloc->memory_small = t_heap_new(\
			MALLOC_MEM_SMALL_LGTH,\
			malloc->data_memory_handler);
	}
	if (!malloc->memory_small)
		return (NULL);
	return (t_heap_allocate(malloc->memory_small, size));
}

static void	*malloc_allocate_large(t_malloc *malloc, size_t size)
{
	if (!malloc->memory_large)
	{
		malloc->memory_large = t_heap_new(\
			T_HEAP__MEMORY_PAGE_SIZE_UNFIXED,\
			malloc->data_memory_handler);
	}
	if (!malloc->memory_large)
		return (NULL);
	return (t_heap_allocate(malloc->memory_large, size));
}

void		*malloc_allocate(t_malloc *malloc, size_t size)
{
	void	*ptr;

	if ((ptr = malloc_allocate_sub(malloc, round_to_mod16(size))))
		allocation_tracker_add(malloc, 1);
	return (ptr);
}

void		*malloc_allocate_sub(t_malloc *malloc, size_t size)
{
	if (is_size_tiny(size))
		return (malloc_allocate_tiny(malloc, size));
	if (is_size_small(size))
		return (malloc_allocate_small(malloc, size));
	return (malloc_allocate_large(malloc, size));
}
