/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_heap.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 15:04:47 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "errors.h"
#include "memory_handler.h"
#include "s_heap.h"

/*
** Constructor
** -----------
*/

t_heap	*t_heap_new(\
	size_t fixed_memory_page_size,\
	t_data_memory_handler *dmh)
{
	t_heap *heap;

	if (!(heap = mmap_handler(sizeof(t_heap))))
	{
		error_handler_set(S_HEAP__UNABLE_TO_ALLOCATE_MEMORY);
		return (NULL);
	}
	return (t_heap_initialize(heap, fixed_memory_page_size, dmh));
}

t_heap	*t_heap_initialize(\
	t_heap *heap,\
	size_t fixed_memory_page_size,\
	t_data_memory_handler *dmh)
{
	heap->fixed_memory_page_size = fixed_memory_page_size;
	heap->next = NULL;
	heap->allocation_counter = 0;
	heap->memory_page_head = NULL;
	heap->memory_page_size = 0;
	heap->lst_data = NULL;
	if (!(heap->data_memory_handler = dmh))
	{
		t_heap_delete(heap);
		return (NULL);
	}
	return (heap);
}

/*
** Destructor
** ----------
*/

void	t_heap_list_delete(t_heap *heap_head)
{
	t_heap	*offset;
	t_heap	*next;

	offset = heap_head;
	while (offset != NULL)
	{
		next = offset->next;
		t_heap_delete(offset);
		offset = next;
	}
}

void	t_heap_delete(t_heap *heap)
{
	if (heap->memory_page_head)
		munmap_handler(heap->memory_page_head, heap->memory_page_size);
	if (munmap_handler(heap, sizeof(heap)))
		error_handler_set(S_HEAP__UNABLE_TO_RELEASE_MEMORY);
}

/*
** Show info
** ---------
*/

void	t_heap_show_info(t_heap *heap, int fd)
{
	t_data	*data_pt;

	print_address_fd(heap->memory_page_head, fd);
	lib_putstr_fd(": ", fd);
	lib_putnbr_fd(heap->memory_page_size, fd);
	lib_putendl_fd(" octets", fd);
	data_pt = heap->lst_data;
	while (data_pt)
	{
		print_data_info(data_pt, "\t", fd);
		data_pt = data_pt->next;
	}
	if (heap->next)
	{
		lib_putendl_fd("", fd);
		lib_putstr_fd("\tnext preallocation: ", fd);
		t_heap_show_info(heap->next, fd);
	}
}
