/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_memory_handler.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:16:33 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data_memory_handler.h"

/*
** Constructor
** -----------
*/

t_data_memory_handler	*t_data_memory_handler_new(int number_of_t_data)
{
	t_data_memory_handler *dmh;

	dmh = (t_data_memory_handler *)mmap_handler(sizeof(t_data_memory_handler));
	if (!dmh)
	{
		error_handler_set(S_DATA_MEMORY_HANDLER__UNABLE_TO_ALLOCATE_MEMORY);
		return (NULL);
	}
	return (t_data_memory_handler_initialize(dmh, number_of_t_data));
}

t_data_memory_handler	*t_data_memory_handler_initialize(\
	t_data_memory_handler *dmh, int number_of_t_data)
{
	size_t	offset;

	dmh->number_of_t_data = number_of_t_data;
	dmh->memory_size = sizeof(t_data) * dmh->number_of_t_data;
	if (!(dmh->memory_head = mmap_handler(dmh->memory_size)))
	{
		t_data_memory_handler_delete(NULL, dmh);
		return (NULL);
	}
	dmh->occupied_spots_length = dmh->memory_size / (dmh->number_of_t_data);
	if (!(dmh->occupied_spots = mmap_handler(dmh->occupied_spots_length)))
	{
		t_data_memory_handler_delete(NULL, dmh);
		return (NULL);
	}
	offset = 0;
	while (offset < dmh->occupied_spots_length)
		dmh->occupied_spots[offset++] = NULL;
	dmh->next = NULL;
	return (dmh);
}

/*
** Destructor
** ----------
*/

/*
** TODO: rename clear by delete
*/

void					t_data_memory_handler_clear(t_data_memory_handler *dmh)
{
	if (dmh->next)
		t_data_memory_handler_clear(dmh->next);
	t_data_memory_handler_delete(NULL, dmh);
}

void					t_data_memory_handler_delete(\
	t_data_memory_handler *previousdmh, t_data_memory_handler *dmh_to_delete)
{
	if (previousdmh)
		previousdmh->next = dmh_to_delete->next;
	if (dmh_to_delete->memory_head)
	{
		if (munmap_handler(\
			dmh_to_delete->memory_head,\
			dmh_to_delete->memory_size))
		{
			error_handler_set(S_DATA__UNABLE_TO_RELEASE_MEMORY);
		}
	}
	if (dmh_to_delete->occupied_spots)
	{
		if (munmap_handler(\
			dmh_to_delete->occupied_spots,\
			dmh_to_delete->occupied_spots_length))
		{
			error_handler_set(S_DATA__UNABLE_TO_RELEASE_MEMORY);
		}
	}
	if (munmap_handler(dmh_to_delete, sizeof(t_data_memory_handler)))
		error_handler_set(S_DATA__UNABLE_TO_RELEASE_MEMORY);
}
