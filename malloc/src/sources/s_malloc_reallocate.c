/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_malloc_reallocate.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 12:07:29 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory_handler.h"
#include "errors.h"
#include "libft.h"
#include "s_malloc.h"

void	*malloc_reallocate(t_malloc *malloc, void *ptr, size_t size_new)
{
	t_data	*data;
	size_t	rd_size_new;

	rd_size_new = round_to_mod16(size_new);
	data = NULL;
	if (NULL == ptr)
		return (malloc_allocate(malloc, rd_size_new));
	else if (malloc->memory_tiny
		&& (data = t_data_find_ptr(malloc->memory_tiny->lst_data, ptr)))
		return (malloc_reallocate_tiny(malloc, data, rd_size_new));
	else if (malloc->memory_small
		&& (data = t_data_find_ptr(malloc->memory_small->lst_data, ptr)))
		return (malloc_reallocate_small(malloc, data, rd_size_new));
	else if (malloc->memory_large
		&& (data = t_data_find_ptr(malloc->memory_large->lst_data, ptr)))
		return (malloc_reallocate_large(malloc, data, rd_size_new));
	else
		return (NULL);
}

void	*malloc_reallocate_tiny(t_malloc *malloc, t_data *data, size_t size_new)
{
	void	*ptr_new;

	if (is_size_tiny(size_new))
	{
		ptr_new = t_heap_reallocate(malloc->memory_tiny, data->ptr, size_new);
		if (error_handler_get() == ERROR_HANDLER__NO_ERROR)
			return (ptr_new);
		else
			return (NULL);
	}
	return (malloc_reallocate_default(malloc, data, size_new));
}

void	*malloc_reallocate_small(\
	t_malloc *malloc,\
	t_data *data, size_t size_new)
{
	void	*ptr_new;

	if (is_size_small(size_new))
	{
		ptr_new = t_heap_reallocate(malloc->memory_small, data->ptr, size_new);
		if (error_handler_get() == ERROR_HANDLER__NO_ERROR)
			return (ptr_new);
		else
			return (NULL);
	}
	return (malloc_reallocate_default(malloc, data, size_new));
}

void	*malloc_reallocate_large(\
	t_malloc *malloc,\
	t_data *data,\
	size_t size_new)
{
	void	*ptr_new;

	if (!is_size_tiny(size_new) && !is_size_small(size_new))
	{
		ptr_new = t_heap_reallocate(malloc->memory_large, data->ptr, size_new);
		if (error_handler_get() == ERROR_HANDLER__NO_ERROR)
			return (ptr_new);
		else
			return (NULL);
	}
	return (malloc_reallocate_default(malloc, data, size_new));
}

void	*malloc_reallocate_default(\
	t_malloc *malloc,\
	t_data *data,\
	size_t size_new)
{
	void	*new_ptr;

	if (!(new_ptr = malloc_allocate(malloc, size_new)))
		return (NULL);
	lib_memcpy2(new_ptr, data->ptr, size_new, data->size);
	malloc_release(malloc, data->ptr);
	return (new_ptr);
}
