/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_data_remove.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 14:44:38 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data.h"

void	t_data_delete_ptr(t_data *lst_data_head, void *ptr)
{
	t_data	*offset;

	if (lst_data_head && lst_data_head->ptr == ptr)
		return (t_data_delete(NULL, lst_data_head));
	offset = lst_data_head;
	while (offset && offset->next)
	{
		if (offset->next->ptr == ptr)
		{
			t_data_delete(offset, offset->next);
			return ;
		}
		offset = offset->next;
	}
	error_handler_set(S_DATA__FIND_PTR__NOT_FOUND);
	return ;
}
