/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_data.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 15:00:37 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_data.h"

/*
** Constructor
** -----------
*/

t_data	*t_data_new(void *ptr, size_t size)
{
	t_data *sdata;

	if (!(sdata = (t_data *)mmap_handler(sizeof(t_data))))
	{
		error_handler_set(S_DATA__UNABLE_TO_ALLOCATE_MEMORY);
		return (NULL);
	}
	return (t_data_initializer(sdata, ptr, size));
}

t_data	*t_data_initializer(t_data *sdata, void *ptr, size_t size)
{
	sdata->next = NULL;
	sdata->size = size;
	sdata->ptr = ptr;
	return (sdata);
}

/*
** Destructor
** ----------
*/

/*
** rename to t_data_delete_lst
*/

void	t_data_clear(t_data *lst_data_head)
{
	if (lst_data_head->next)
		t_data_clear(lst_data_head->next);
	t_data_delete(NULL, lst_data_head);
}

/*
** t_data_delete
**
** variable:
** _________ previous_data: data before the element to delete or NULL.
*/

void	t_data_delete(t_data *previous_data, t_data *data_to_delete)
{
	if (previous_data)
	{
		previous_data->next = data_to_delete->next;
	}
	if (munmap_handler(data_to_delete, sizeof(t_data)))
		error_handler_set(S_DATA__UNABLE_TO_RELEASE_MEMORY);
}

/*
** Show info
** ---------
*/

void	print_data_info(t_data *data, char *prefix, int fd)
{
	lib_putstr_fd(prefix, fd);
	print_address_fd(data->ptr, fd);
	lib_putstr_fd(" - ", fd);
	print_address_fd(data->ptr + data->size, fd);
	lib_putstr_fd(": ", fd);
	lib_putnbr_fd(data->size, fd);
	lib_putendl_fd(" octets", fd);
}
