/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_heap_reallocate.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 14:36:47 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_heap.h"

void		*t_heap_reallocate(t_heap *heap_head, void *ptr, size_t size_new)
{
	error_handler_reset();
	return (t_heap_reallocate_sub(heap_head, heap_head, ptr, size_new));
}

static void	*t_heap_reallocate_element(\
	t_heap *heap_head,\
	t_heap *heap,\
	t_data *offset,\
	size_t size_new)
{
	void	*ptr_new;

	if ((offset->next && is_block_big_enough(\
		offset->ptr,\
		offset->next->ptr,\
		size_new))
		|| (!offset->next && is_block_big_enough(\
			offset->ptr,\
			heap->memory_page_head + heap->memory_page_size,\
			size_new)))
	{
		offset->size = size_new;
		return (offset->ptr);
	}
	else
	{
		if (!(ptr_new = t_heap_allocate(heap_head, size_new)))
			return (NULL);
		stc_memcpy(ptr_new, offset->ptr, offset->size);
		t_heap_list_release(heap_head, offset->ptr);
		return (ptr_new);
	}
}

void		*t_heap_reallocate_sub(\
	t_heap *heap_head,\
	t_heap *heap,\
	void *ptr,\
	size_t size_new)
{
	t_data	*offset;

	if (heap && heap->lst_data)
	{
		offset = heap->lst_data;
		while (offset)
		{
			if (offset->ptr == ptr)
			{
				return (t_heap_reallocate_element(\
					heap_head,\
					heap,\
					offset,\
					size_new));
			}
			offset = offset->next;
		}
	}
	if (heap && heap->next)
		return (t_heap_reallocate_sub(heap_head, heap->next, ptr, size_new));
	return (NULL);
}

void		*stc_memcpy(void *dst, const void *src, size_t n)
{
	unsigned char	*s1;
	unsigned char	*s2;

	s1 = (unsigned char *)src;
	s2 = (unsigned char *)dst;
	if (dst == src || n == 0)
		return (s2);
	while (n--)
		*s2++ = *s1++;
	return ((void *)dst);
}
