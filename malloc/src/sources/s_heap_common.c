/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_heap_common.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:50:16 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "s_heap.h"

int		is_block_big_enough(void *ptr1, void *ptr2, size_t size)
{
	return (ptr2 - ptr1 >= (int)size);
}

void	t_heap_allocation_add(t_heap *heap, int add)
{
	heap->allocation_counter += add;
}
