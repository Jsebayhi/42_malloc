/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_itoa_buffered.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/03/30 12:44:54 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <limits.h>
#include "libft.h"

#define L_INT_MIN "-2147483648"
#define L_INT_MIN_SIZE lib_strlen(L_INT_MIN)

static size_t	lib_compute_length(int n)
{
	size_t len;

	len = 1;
	if (n > 0)
	{
		len = 0;
		n *= -1;
	}
	while (n != 0)
	{
		n /= 10;
		len += 1;
	}
	return (len);
}

static size_t	lib_stat_length(int n, size_t buffer_size)
{
	size_t	len;

	len = lib_compute_length(n);
	if (buffer_size <= len)
	{
		return (buffer_size);
	}
	return (len);
}

int				lib_itoa_buffered(int n, char *buffer, size_t buffer_size)
{
	size_t		len;
	size_t		i;

	if (n == INT_MIN)
	{
		buffer = lib_strncpy(buffer, L_INT_MIN, buffer_size);
		return (L_INT_MIN_SIZE > buffer_size ? buffer_size : L_INT_MIN_SIZE);
	}
	len = lib_stat_length(n, buffer_size);
	if (n < 0)
	{
		buffer[0] = '-';
		n *= -1;
	}
	if (n == 0)
		buffer[0] = '0';
	i = 0;
	while (n != 0 && i < buffer_size)
	{
		buffer[len - i - 1] = n % 10 + '0';
		i++;
		n /= 10;
	}
	buffer[len] = '\0';
	return (len);
}
