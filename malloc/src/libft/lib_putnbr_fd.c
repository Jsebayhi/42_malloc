/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lib_putnbr_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/07/09 15:53:39 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/03/30 12:46:59 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

/*
** should be:
** size_t	buffer_size = 11;
** char		buffer[buffer_size];
*/

void	lib_putnbr_fd(int n, int fd)
{
	size_t	buffer_size;
	char	buffer[11];

	buffer_size = 11;
	lib_itoa_buffered(n, buffer, buffer_size);
	lib_putstr_fd(buffer, fd);
}
