/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   conf.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:40:07 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CONF_H
# define CONF_H

# define FD_INFO 1
# ifndef FD_DEBUG
#  define FD_DEBUG 2
# endif
/*
** 1: print debug; 0: shut up.
*/
# define DEBUG 0
# define DEBUG_WITH_DETAIL 0
# define FLAG_SHOW_ALLOCATION_COUNTER 0

/*
** Number of t_data that you want to be able to allocate per
** t_data_memory_handler.
*/
# define DMH_SIZE 1000

#endif
