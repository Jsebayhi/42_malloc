/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_data.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/03/30 15:49:38 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef S_DATA_H
# define S_DATA_H
# include "conf.h"
# include "standalone_functions.h"
# include "libft.h"
# include "errors.h"
# include "memory_handler.h"

/*
** Linked list sorted by address in creasing order.
** ptr is the pointer to the allocated memory
*/
typedef struct		s_data
{
	struct s_data	*next;
	void			*ptr;
	size_t			size;
}					t_data;

/*
** Allocate memory and initialize the t_data.
*/
t_data				*t_data_new(void *ptr, size_t size);
t_data				*t_data_initializer(t_data *sdata, void *ptr, size_t size);
void				t_data_delete(\
	t_data *lst_data_head,\
	t_data *data_to_delete);
void				t_data_clear(t_data *lst_data_head);
t_data				*t_data_insert_before(t_data *data, t_data *data_new);
void				t_data_insert_after(t_data *previous_data, \
	t_data *data_new);
void				print_data_info(t_data *data, char *prefix, int fd);

/*
** return:
** _______ The found data.
** _______ NULL if an error occured or no match.
*/
t_data				*t_data_find_ptr(t_data *lst_data_head, void *ptr);
t_data				*t_data_register_new_data(\
	t_data *lst_data_head,\
	t_data *data_new);

#endif
