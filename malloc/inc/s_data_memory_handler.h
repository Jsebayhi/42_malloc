/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_data_memory_handler.h                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/13 16:20:09 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef S_DATA_MEMORY_HANDLER_H
# define S_DATA_MEMORY_HANDLER_H
# include "libft.h"
# include "errors.h"
# include "memory_handler.h"
# include "s_data.h"

/*
** TODO: replace memory_head and memory_size by a t_page for clarity's sake.
**
** Handle preallocated memory partition which it uses to give memory to t_data.
**
** occupied_spots:        store a pointer to the t_data the memory is
**                        allocated to
** occupied_spots_length: size of memory_size / sizeof(t_data)
*/
typedef struct						s_data_memory_handler
{
	int								number_of_t_data;
	void							*memory_head;
	size_t							memory_size;
	void							**occupied_spots;
	size_t							occupied_spots_length;
	struct s_data_memory_handler	*next;
}									t_data_memory_handler;

t_data_memory_handler				*t_data_memory_handler_new(\
	int number_of_t_data);
t_data_memory_handler				*t_data_memory_handler_initialize(\
	t_data_memory_handler *dmh, int number_of_t_data);
void								t_data_memory_handler_delete(\
	t_data_memory_handler *previousdmh, t_data_memory_handler *dmh_to_delete);
void								t_data_memory_handler_clear(\
	t_data_memory_handler *dmh);
/*
** check if there is space to register a new t_data and append a new
** s_data_memory_handler if needs be.
*/
t_data								*allocate_new_t_data(\
	t_data_memory_handler *dmh, void *ptr, size_t size);
/*
** Set the memory of the t_data as free. Delete the s_data_memory_handler
**  if no more pointer are registered on it,
**  if it is the last one in the list
**  and if another one exists.
*/
void								release_t_data(\
	t_data_memory_handler *dmh, t_data *data);

#endif
