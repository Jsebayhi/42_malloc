/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_heap.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/03/30 15:52:32 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef S_HEAP_H
# define S_HEAP_H
# include "libft.h"
# include "errors.h"
# include "memory_handler.h"
# include "s_data.h"
# include "s_data_memory_handler.h"

# define T_HEAP__MEMORY_PAGE_SIZE_UNFIXED 0

/*
** fields:
** _______ fixed_memory_page_size: the size to use when creating new page or
**          T_PAGE__MEMORY_PAGE_SIZE_UNFIXED if you want to allocate pages on
**          the fly.
** _______ allocation_counter: number of memory allocation active on this page.
*/
typedef struct				s_heap
{
	size_t					fixed_memory_page_size;
	struct s_heap			*next;

	unsigned int			allocation_counter;
	void					*memory_page_head;
	size_t					memory_page_size;
	t_data					*lst_data;
	t_data_memory_handler	*data_memory_handler;
}							t_heap;

void						t_heap_allocation_add(t_heap *heap, int add);
t_heap						*t_heap_initialize(\
	t_heap *heap,\
	size_t fixed_memory_page_size,\
	t_data_memory_handler *dmh);
t_heap						*t_heap_memory_page_initialize(\
	t_heap *heap,\
	size_t target_size);
void						*t_heap_reallocate_sub(\
	t_heap *heap_head,\
	t_heap *heap,\
	void *ptr,\
	size_t size_new);
int							t_heap_release(t_heap *heap, void *ptr);
void						t_heap_delete(t_heap *heap);
int							is_block_big_enough(\
	void *ptr1,\
	void *ptr2,\
	size_t size);
void						*t_heap_find_available_block_of_size(\
	t_heap *heap,\
	size_t size);
t_heap						*t_heap_new(\
	size_t fixed_memory_page_size, t_data_memory_handler *dmh);
void						t_heap_list_delete(t_heap *heap_head);

void						*t_heap_allocate(t_heap *heap, size_t size);
void						*t_heap_reallocate(\
	t_heap *heap_head, void *ptr, size_t size_new);
void						t_heap_list_release(\
	t_heap *heap_head,\
	void *ptr);
void						t_heap_show_info(t_heap *heap, int fd);
void						*stc_memcpy(void *dst, const void *src, size_t n);
#endif
