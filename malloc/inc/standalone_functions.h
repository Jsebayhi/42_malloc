/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   standalone_functions.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 15:00:14 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STANDALONE_FUNCTIONS_H
# define STANDALONE_FUNCTIONS_H

int		print_memory_str_size();
void	print_memory(void *p, char *str);

void	print_address_fd(void *ptr, int fd);
void	print_address_endl_fd(void *ptr, int fd);
#endif
