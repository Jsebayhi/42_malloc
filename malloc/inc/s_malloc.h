/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   s_malloc.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 12:32:08 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef S_MALLOC_H
# define S_MALLOC_H
# include "libft.h"
# include "s_heap.h"

/*
** MALLOC_MEMORY_TINY
*/
# define MALLOC_MEM_T_SIZE (size_t) (getpagesize() / 4)
# define MALLOC_MEM_T_DEPTH 100
# define MALLOC_MEM_TINY_LGTH (size_t)(MALLOC_MEM_T_SIZE * MALLOC_MEM_T_DEPTH)

/*
** MALLOC_MEMORY_SMALL
*/
# define MALLOC_MEM_S_SIZE MALLOC_MEM_T_SIZE * 2
# define MALLOC_MEM_S_DEPTH 100
# define MALLOC_MEM_SMALL_LGTH (size_t)(MALLOC_MEM_S_SIZE * MALLOC_MEM_S_DEPTH)

typedef struct				s_malloc
{
	int						allocation_tracker;
	t_heap					*memory_tiny;
	t_heap					*memory_small;
	t_heap					*memory_large;
	t_data_memory_handler	*data_memory_handler;
	unsigned int			has_been_initialized;
}							t_malloc;

/*
** Initiallize a t_malloc.
** return:
** _______ An initialized, run-ready t_malloc.
** _______ NULL if an error occured.
*/
t_malloc					*malloc_new();
t_malloc					*malloc_initialize(t_malloc	*malloc);
void						malloc_delete(t_malloc *malloc);
void						*malloc_allocate(t_malloc *malloc, size_t size);
void						*malloc_reallocate(\
	t_malloc *malloc, void *ptr, size_t size_new);
void						malloc_release(t_malloc *malloc, void *ptr);
void						malloc_show_info(t_malloc *malloc, int fd);

/*
** Internal functions
** ------------------
*/
t_malloc					*get_malloc(void);
int							is_size_tiny(size_t	size);
int							is_size_small(size_t size);
void						allocation_tracker_add(t_malloc *malloc, int add);
size_t						round_to_mod16(size_t size);
void						*lib_memcpy2(\
	void *dst,\
	const void *src,\
	size_t dst_size,\
	size_t src_size);
void						*malloc_allocate_sub(\
	t_malloc *malloc,\
	size_t size);
void						*malloc_reallocate_tiny(\
	t_malloc *malloc,\
	t_data *data,\
	size_t size_new);
void						*malloc_reallocate_small(\
	t_malloc *malloc,\
	t_data *data,\
	size_t size_new);
void						*malloc_reallocate_large(\
	t_malloc *malloc,\
	t_data *data,\
	size_t size_new);
void						*malloc_reallocate_default(\
	t_malloc *malloc,\
	t_data *data,\
	size_t size_new);
#endif
