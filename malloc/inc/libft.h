/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 17:02:24 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/03/23 13:10:05 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <ctype.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>
# include <limits.h>

float				lib_atof(char *str);
int					lib_atoi(const char *str);

void				lib_bzero(void *s, size_t n);
void				*lib_memccpy(void *dst, const void *src, int c, size_t ln);
void				*lib_memchr(const void *s, int c, size_t n);
int					lib_memcmp(const void *s1, const void *s2, size_t n);
void				*lib_memcpy(void *dst, const void *src, size_t n);
void				*lib_memset(void *b, int c, size_t len);
char				*lib_strcat(char *s1, const char *s2);
char				*lib_strchr(const char *s, int c);
int					lib_strcmp(const char *s1, const char *s2);
char				*lib_strcpy(char *dst, const char *src);
size_t				lib_strlcat(char *dst, const char *src, size_t size);
size_t				lib_strlen(const char *str);
char				*lib_strncat(char *s1, const char *s2, size_t n);
int					lib_strncmp(const char *s1, const char *s2, size_t n);
char				*lib_strncpy(char *dst, const char *src, size_t n);
char				*lib_strnstr(const char *s1, const char *s2, size_t n);
char				*lib_strrchr(const char *s, int c);
char				*lib_strstr(const char *s1, const char *s2);

void				lib_putendl(const char *s);
void				lib_putendl_fd(const char *s, int fd);
void				lib_putstr(char const *s);
void				lib_putstr_fd(char const *s, int fd);
void				lib_putnbr(int n);
void				lib_putnbr_fd(int n, int fd);

char				*lib_itoa(int n);
int					lib_itoa_buffered(int n, char *buffer, size_t buffer_size);

#endif
