/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocation_counter.h                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 13:32:12 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ALLOCATION_COUNTER_H
# define ALLOCATION_COUNTER_H

# define SHOW_ALLOCATION_COUNTER 1

void	allocation_counter_set(int n);
int		allocation_counter_get(void);
void	allocation_counter_show(void);
int		allocation_counter_add(int add);

#endif
