/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:06:34 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H
# include "libft.h"
# include "malloc_status.h"
# include "malloc_struct.h"

void	*malloc(size_t size);
void	*malloc_sub(size_t size);
void	*calloc(size_t block, size_t size);
void	*calloc_sub(size_t block, size_t size);
void	*realloc(void *ptr, size_t size);
void	*realloc_sub(void *ptr, size_t size);
void	free(void *ptr);
void	free_sub(void *ptr);

/*
** show the status of the prealocated memory
*/

void	show_alloc_mem();
void	show_alloc_mem_sub();

#endif
