/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memory_handler.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:03:50 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MEMORY_HANDLER_H
# define MEMORY_HANDLER_H
# include "libft.h"
# include "allocation_counter.h"
# include "errors.h"
# include <sys/mman.h>
# include <sys/types.h>
# include <stdio.h>
# include <errno.h>

void	*mmap_handler(size_t size);
int		munmap_handler(void *addr, size_t length);
size_t	round_to_page_size_multiple(size_t size);

#endif
