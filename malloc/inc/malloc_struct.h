/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_struct.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 14:03:43 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_STRUCT_H
# define MALLOC_STRUCT_H

# include "errors.h"
# include "s_malloc.h"

void		uninitialize_malloc_struct(void);
t_malloc	*get_malloc_struct(void);

#endif
