/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jsebayhi <jsebayhi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/20 18:30:57 by jsebayhi          #+#    #+#             */
/*   Updated: 2019/04/21 12:28:03 by jsebayhi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERRORS_H
# define ERRORS_H
# include "conf.h"
# include "libft.h"

int		error_handler_get();
int		error_handler_set(int error_code);
int		error_handler_reset();
void	debug();

/*
** ERROR CODE and MESSAGE
** ----------------------
*/

/*
** X - ERROR_HANDLER
*/
# define ERROR_HANDLER__DEFAULT_ERROR 0
# define ERROR_HANDLER__NO_ERROR ERROR_HANDLER__DEFAULT_ERROR
# define ERROR_HANDLER__APPLY 1
# define ERROR_HANDLER__DO_NOT_APPLY_CODE 0

/*
** 1XX - MMap error
*/

# define MMAP__UNABLE_TO_ALLOCATE_MEMORY 101

/*
** 2XX - MunMap error
*/

# define MUNMAP__UNABLE_TO_RECLAIM_MEMORY 201

/*
** 3XX - S_DATA
*/

# define S_DATA__UNDEFINED_ERROR 300
# define S_DATA__UNABLE_TO_ALLOCATE_MEMORY 301
# define S_DATA__UNABLE_TO_RELEASE_MEMORY 302
# define S_DATA__FIND_PTR__NOT_FOUND 311

/*
** 5XX - S_DATA_MEMORY_HANDLER
*/

# define S_DATA_MEMORY_HANDLER__UNABLE_TO_ALLOCATE_MEMORY 501
# define S_DATA_MEMORY_HANDLER__UNABLE_TO_RELEASE_MEMORY 502
# define S_DATA_MEMORY_HANDLER__T_DATA_NOT_FOUND 511

/*
** 6XX - S_HEAP
*/

# define S_HEAP__UNDEFINED_ERROR 600
# define S_HEAP__UNABLE_TO_ALLOCATE_MEMORY 601
# define S_HEAP__UNABLE_TO_RELEASE_MEMORY 602
# define S_HEAP__FIND_PTR__NOT_FOUND 611

/*
** 7XX - S_MALLOC
*/

# define S_MALLOC__UNABLE_TO_RELEASE_MEMORY 701

/*
** 8XX - MALLOC_STATUS
*/

# define MALLOC_STATUS__ALREADY_INITIALIZED 801
# define MALLOC_STATUS__ALREADY_NOT_INITIALIZED 802

#endif
